#pragma once

#include <stdlib.h>
#include <string.h>

typedef struct {
	char *str;
	size_t len;
	size_t index;
} Buffer;

void initBuffer(Buffer *buf);
void addChar(char c, Buffer *buf);

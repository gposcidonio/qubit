#pragma once

#define MAX_PORT ((1 << 16) - 1)
#define MAX_COMMAND_LEN 1001
#define MAX_USERNAME_LEN 20

#define CLIENT_ALOHA "ALOHA! \r\n"
#define CLIENT_ALOHA_LEN (sizeof("ALOHA! \r\n") - 1)
#define SERVER_AHOLA "!AHOLA \r\n"
#define SERVER_AHOLA_LEN (sizeof("!AHOLA \r\n") - 1)

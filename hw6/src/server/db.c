#include "db.h"

ClientAuth *new_ClientAuth() {
	ClientAuth *auth = malloc(sizeof(ClientAuth));
	auth->username = NULL;
	auth->passHash = NULL;
	return auth;
}

void free_ClientAuth(ClientAuth *auth) {
	if (auth->username != NULL) free(auth->username);
	if (auth->passHash != NULL) free(auth->passHash);
	free(auth);
}

int initDb(sqlite3 **pdb) {
	int ret = sqlite3_open("auth-db", pdb);
	if (ret != 0) {
		fprintf(
			stderr,
			"Can't open the database: %s\n",
			sqlite3_errmsg(*pdb)
		);
		sqlite3_close(*pdb);
		return 1;
	}

	char *errMsg;
	ret = sqlite3_exec(
		*pdb,
		"CREATE TABLE IF NOT EXISTS users("
		"	name      TEXT  NOT NULL,"
		"	passHash  TEXT  NOT NULL,"
		"	salt      TEXT  NOT NULL"
		");",
		NULL,
		NULL,
		&errMsg
	);
	if (ret != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", errMsg);
		sqlite3_free(errMsg);
		return 1;
	}

	return 0;
}

ClientAuth *getClientAuth(sqlite3 *db, char *name) {
	ClientAuth *auth = NULL;
	char *errMsg;
	int ret = sqlite3_exec(
		db,
		sqlite3_mprintf("SELECT * FROM users WHERE name=%Q", name),
		getClientAuthCallback,
		&auth,
		&errMsg
	);
	if (ret != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", errMsg);
		sqlite3_free(errMsg);
		return NULL;
	}

	return auth;
}

int getClientAuthCallback(
	void *arg,
	int argc,
	char **argv,
	char **colName
) {
	ClientAuth **pauth = arg;
	if (argc > 0) {
		ClientAuth *auth = new_ClientAuth();
		auth->username = malloc(strlen(argv[0]));
		strcpy(auth->username, argv[0]);
		auth->passHash = malloc(strlen(argv[1]));
		strcpy(auth->passHash, argv[1]);
		strncpy(auth->salt, argv[2], SALT_LEN);
		*pauth = auth;
	} else {
		*pauth = NULL;
	}

	return 0;
}

int addClientAuth(sqlite3 *db, ClientAuth *auth) {
	char salt[SALT_LEN + 1];
	strncpy(salt, auth->salt, SALT_LEN);
	salt[SALT_LEN] = '\0';

	char *errMsg;
	int ret = sqlite3_exec(
		db,
		sqlite3_mprintf(
			"INSERT INTO users (name,passHash,salt) "
			"VALUES (%Q,%Q,%Q)",
			auth->username,
			auth->passHash,
			salt
		),
		NULL,
		NULL,
		&errMsg
	);

	if (ret != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s", errMsg);
		sqlite3_free(errMsg);
		return -1;
	}

	return 0;
}

void cleanupDb(sqlite3 **pdb) {
	sqlite3_close(*pdb);
	*pdb = NULL;
}

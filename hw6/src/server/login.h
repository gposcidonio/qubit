#pragma once

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <openssl/rand.h>

#include "db.h"
#include "io.h"
#include "echo.h"
#include "state.h"
#include "client-list.h"
#include "../io.h"

#define SHA_DIGEST_LEN 20

typedef struct {
	ServerState *state;
	int clientFd;
} LoginStruct;

void *login(void *loginStruct);
int loginClient(ServerState *state, ClientList *client, char *name);
int registerClient(ServerState *state, ClientList *client, char *name);
int isValidPassword(char *password);

char *hashPassword(char *password, char *salt);
int isRegistered(char *name);
void logConnection(ServerState *state, ClientList *client, int error);

#pragma once

#include <errno.h>
#include <stdio.h>

#include "../io.h"
#include "state.h"
#include "parse.h"

// arg is struct ServerState *
void *echo(void *arg);

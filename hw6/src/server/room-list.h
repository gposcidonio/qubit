#pragma once

#include <assert.h>
#include <stdlib.h>

#include "client-list.h"
#include "id-list.h"

typedef struct RoomList {
	int isPrivate;
	char *password;
	int ownerId;
	int roomId;
	char *roomName;
	IDList *clientIds;
	struct RoomList *next;
	struct RoomList *prev;
} RoomList;

RoomList *new_RoomList(int roomId, int ownerId, char *roomName);
void free_RoomList(RoomList *list);


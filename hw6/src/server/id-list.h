#pragma once

#include <assert.h>
#include <stdlib.h>

typedef struct IDList {
	int id;
	struct IDList *next;
} IDList;

IDList *new_IDList(int id);
void free_IDList(IDList *node);
int removeIDFromIDList(int id, IDList **list);

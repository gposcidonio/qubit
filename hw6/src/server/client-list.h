#pragma once

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include "../buffer.h"

#define SALT_LEN 5

typedef struct ClientList {
	int clientId;
	int roomId;
	int fd;
	char *username;
	int shouldBeRemoved;
	Buffer buf;
	struct ClientList *next;
	struct ClientList *prev;
} ClientList;

ClientList *new_ClientList(int fd);
void free_ClientList(ClientList *clientList);

#include "quit.h"

void quit(ServerState *state, int retVal) {
	free_ServerState(state);
	exit(retVal);
}

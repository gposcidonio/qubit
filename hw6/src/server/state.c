#include "state.h"

ServerState *new_ServerState() {
	ServerState *state = malloc(sizeof(ServerState));
	state->port = -1;
	state->echoToStdout = 0;
	state->listenFd = -1;
	state->clients = NULL;
	state->rooms = NULL;
	state->numClients = 0;
	state->numRooms = 0;
	state->maxRooms = 5;
	state->echoThreadRunning = 0;
	pthread_mutex_init(&state->echoMutex, NULL);
	pthread_mutex_init(&state->roomMutex, NULL);
	pthread_mutex_init(&state->userMutex, NULL);
	initDb(&state->db);
	return state;
}

void free_ServerState(ServerState *state) {
	if (state->listenFd >= 0) close(state->listenFd);
	free(state);
}

void addClient(ServerState *state, ClientList *client) {
	client->clientId = getFreeClientId(state);
	if (state->clients == NULL) {
		client->next = client;
		client->prev = client;
	} else {
		client->next = state->clients;
		client->prev = state->clients->prev;
		state->clients->prev->next = client;
		state->clients->prev = client;
	}

	state->numClients++;
	state->clients = client;
}

void addRoom(volatile ServerState *state, RoomList *room) {
	if (state->rooms == NULL) {
		room->next = room;
		room->prev = room;
	} else {
		room->next = state->rooms;
		room->prev = state->rooms->prev;
		state->rooms->prev->next = room;
		state->rooms->prev = room;
		state->rooms = room;
	}

	state->numRooms++;
	state->rooms = room;
}

void removeClient(volatile ServerState *state, ClientList *client) {
	close(client->fd);
	if (client->next == client) {
		state->clients = NULL;
	} else {
		client->prev->next = client->next;
		client->next->prev = client->prev;
		if (state->clients == client) {
			state->clients = client->next;
		}
	}

	RoomList *room = getRoomForId(state, client->roomId);
	if(room != NULL){
		removeIDFromIDList(client->clientId, &room->clientIds);
		if(room->clientIds == NULL){
			removeRoom(state, room);
		}
	}

	state->numClients--;
	free_ClientList(client);
}

void removeRoom(volatile ServerState *state, RoomList *room) {
	if(room->next == room) {
		state->rooms = NULL;
	} else {
		room->prev->next = room->next;
		room->next->prev = room->prev;
		if (state->rooms == room) {
			state->rooms = room->next;
		}
	}
	state->numRooms--;
	free_RoomList(room);
}

int getFreeRoomId(volatile ServerState *state){
	RoomList *firstRoom = state->rooms;
	RoomList *curRoom = firstRoom;
	int id = 1;
	int numIterations = 0;

	if(curRoom == NULL){
		return id;
	}

	do{
		if(curRoom->roomId == id){
			id++;
			numIterations = 0;
			continue;
		}
		numIterations++;
		curRoom = curRoom->next;
	} while (numIterations < state->numRooms);

	if(numIterations == state->maxRooms) return -1;

	return id;
}

int getFreeClientId(volatile ServerState *state){
	ClientList *firstClient = state->clients;
	ClientList *curClient = firstClient;
	int id = 1;
	int numIterations = 0;

	if(curClient == NULL){
		return id;
	}

	do{
		if(curClient->clientId == id){
			id++;
			numIterations = 0;
			continue;
		}
		numIterations++;
		curClient = curClient->next;
	} while (numIterations < state->numClients);

	return id;
}

RoomList *getRoomForId(volatile ServerState *state, int roomId){
	if(state->rooms == NULL) return NULL;
	RoomList *firstRoom = state->rooms;
	RoomList *curRoom = firstRoom;

	do {
		if(curRoom->roomId == roomId) return curRoom;

		curRoom = curRoom->next;
	} while(curRoom != firstRoom);

	return NULL;
}

ClientList *getClientForId(volatile ServerState *state, int clientId){
	if(state->clients == NULL) return NULL;
	ClientList *firstClient = state->clients;
	ClientList *curClient = firstClient;

	do {
		if(curClient->clientId == clientId) return curClient;

		curClient = curClient->next;
	} while(curClient != firstClient);

	return NULL;
}

ClientList *getClientForUsername(volatile ServerState *state, char* username){
	if(state->clients == NULL) return NULL;
	ClientList *firstClient = state->clients;
	ClientList *curClient = firstClient;

	do {
		if(strcmp(curClient->username, username) == 0) return curClient;

		curClient = curClient->next;
	} while(curClient != firstClient);

	return NULL;
}

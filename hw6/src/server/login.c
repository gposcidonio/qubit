#include "login.h"

void *login(void *arg) {
	LoginStruct *loginStruct = arg;
	ServerState *state = loginStruct->state;
	ClientList *client = new_ClientList(loginStruct->clientFd);
	free(loginStruct);

	printf("starting login of new client\n");

	// get and check aloha
	char *line = dgetln(client->fd);
	if (strcmp(line, CLIENT_ALOHA) != 0) {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		free_ClientList(client);
		free(line);
		return NULL;
	}
	free(line);

	// send ahola
	sendMessage(client, SERVER_AHOLA);

	// process IAM or IAMNEW
	Command cmd = dgetCommand(client->fd);
	int wasError = 0;
	if (strcmp(cmd.verb, "IAM") == 0) {
		wasError = !loginClient(state, client, cmd.message);
	} else if (strcmp(cmd.verb, "IAMNEW") == 0) {
		wasError = !registerClient(state, client, cmd.message);
	} else {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		wasError = 1;
	}

	free(cmd.verb);
	if (wasError) {
		free_ClientList(client);
		return NULL;
	}

	sendMessage(client, "HI %s \r\n", client->username);

	// send motd
	sendMessage(client, "ECHO server %s \r\n", state->motd);

	pthread_mutex_lock(&state->echoMutex);
	if (!state->echoThreadRunning) {
		state->echoThreadRunning = 1;
		pthread_t tid;
		pthread_create(&tid, NULL, echo, state);
	}
	pthread_mutex_unlock(&state->echoMutex);

	return NULL;
}

int loginClient(ServerState *state, ClientList *client, char *name) {
	//open log file for appending
	client->username = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(client->username, name);

	// check if client exists
	ClientAuth *auth = getClientAuth(state->db, name);
	if (auth == NULL) {
		sendError(client, USER_DOES_NOT_EXIST);
		sendMessage(client, "BYE \r\n");
		logConnection(state, client, USER_DOES_NOT_EXIST);
		return 0;
	}

	// check if client is already logged in
	pthread_mutex_lock(&state->userMutex);
	if (getClientForUsername(state, name) != NULL) {
		sendError(client, SORRY);
		sendMessage(client, "BYE \r\n");
		logConnection(state, client, SORRY);
		pthread_mutex_unlock(&state->userMutex);
		return 0;
	}
	pthread_mutex_unlock(&state->userMutex);

	// get password
	sendMessage(client, "AUTH %s \r\n", name);
	Command cmd = dgetCommand(client->fd);
	if (strcmp(cmd.verb, "PASS") != 0) {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		free(cmd.verb);
		logConnection(state, client, INVALID_OPERATION);
		return 0;
	}

	// check password
	char *password = cmd.message;
	char *hash = hashPassword(password, auth->salt);
	if (strncmp(hash, auth->passHash, SHA_DIGEST_LEN) != 0) {
		sendError(client, INVALID_PASSWORD);
		sendMessage(client, "BYE \r\n");
		free(cmd.verb);
		logConnection(state, client, INVALID_PASSWORD);
		return 0;
	}
	free_ClientAuth(auth);

	// log client in
	printf("client %s connected\n", name);
	pthread_mutex_lock(&state->userMutex);
	addClient(state, client);
	pthread_mutex_unlock(&state->userMutex);
	logConnection(state, client, -1);

	return 1;
}

int registerClient(ServerState *state, ClientList *client, char *name) {
	// check if client already exists
	ClientAuth *auth = getClientAuth(state->db, name);
	client->username = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(client->username, name);
	if (auth != NULL) {
		sendError(client, USER_EXISTS);
		sendMessage(client, "BYE \r\n");
		free_ClientAuth(auth);
		logConnection(state, client, USER_EXISTS);
		return 0;
	}

	if(strlen(name) > MAX_USERNAME_LEN){
		sendError(client, SORRY);
		sendMessage(client, "BYE \r\n");
		logConnection(state, client, SORRY);
		return 0;
	}

	sendMessage(client, "HINEW %s \r\n", name);
	auth = new_ClientAuth();
	auth->username = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(auth->username, name);

	// get password
	Command cmd = dgetCommand(client->fd);
	if (strcmp(cmd.verb, "NEWPASS") != 0) {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		logConnection(state, client, INVALID_OPERATION);
		return 0;
	}

	// validate password
	if (!isValidPassword(cmd.message)) {
		sendError(client, INVALID_PASSWORD);
		sendMessage(client, "BYE \r\n");
		logConnection(state, client, INVALID_PASSWORD);
		return 0;
	}

	// hash password
	if (
		RAND_bytes(
			(unsigned char *)auth->salt,
			SALT_LEN
		) != 1
	) {
		sendError(client, INTERNAL_SERVER_ERROR);
		sendMessage(client, "BYE \r\n");
		return 0;
	}
	char *hash = hashPassword(cmd.message, auth->salt);
	auth->passHash = malloc(SHA_DIGEST_LEN);
	strncpy(auth->passHash, hash, SHA_DIGEST_LEN);
	pthread_mutex_lock(&state->userMutex);
	addClient(state, client);
	pthread_mutex_unlock(&state->userMutex);
	addClientAuth(state->db, auth);
	free_ClientAuth(auth);

	logConnection(state, client, -1);
	return 1;
}

int isValidPassword(char *password) {
	size_t len = 0;
	int uppercases = 0;
	int symbols = 0;
	int numbers = 0;

	while (*password) {
		len++;
		char c = (*(password++));
		if (0x40 < c && c <= 0x5a) {
			uppercases |= 1;
		} else if (0x30 <= c && c < 0x3a) {
			numbers |= 1;
		} else if (c < 0x61 || 0x7a < c) {
			symbols |= 1;
		}
	}

	return len >= 5 && uppercases && symbols && numbers;
}

char *hashPassword(char *password, char *salt) {
	size_t len = strlen(password) + SALT_LEN;
	char *saltedPass = malloc(len + 1);
	strcpy(saltedPass, password);
	strncat(saltedPass, salt, SALT_LEN);

	char *hash = (char *)SHA256(
		(unsigned char *)saltedPass,
		len,
		NULL
	);

	free(saltedPass);
	return hash;
}

int isRegistered(char *name) {
	return 0;
}

void logConnection(ServerState *state, ClientList *client, int error){
	pthread_mutex_lock(&state->logFileMutex);
	FILE *fp = fopen("auth.log", "a");
	//Get the time string into a variable.
	time_t rawTime;
	struct tm* timeInfo;
	time(&rawTime);
	timeInfo = localtime(&rawTime);
	char* timeDesc = malloc(128);
	memset(timeDesc, '\0', 128);
	strftime(timeDesc, 128, "%D - %R%p", timeInfo);

	//compose the correct string to print depending on error
	//Error is a bit of a backwards variable. If an error
	//occurred, then error will hold the error's number.
	//Otherwise, if the connection was successful, error
	//will hold -1.
	char* errorDesc = malloc(128);
	memset(errorDesc, '\0', 128);
	if(error != -1){
		//error occurred
		sprintf(errorDesc, "Failure ERR %d", error);
	} else {
		sprintf(errorDesc, "Success");
	}

	//get the ip address given the file descriptor
	struct sockaddr_in addr;
	socklen_t addr_size = sizeof(struct sockaddr_in);
	getpeername(client->fd, (struct sockaddr*)&addr, &addr_size);
	char* clientip = malloc(32);
	memset(clientip, '\0', 32);
	strcpy(clientip, inet_ntoa(addr.sin_addr));

	fprintf(fp, "#%s\nConnection: %s\nUsername: %s\n%s\n\n", timeDesc, clientip, client->username, errorDesc);

	free(timeDesc);
	free(errorDesc);
	free(clientip);
	fclose(fp);
	pthread_mutex_unlock(&state->logFileMutex);
}

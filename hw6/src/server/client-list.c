#include "client-list.h"

ClientList *new_ClientList(int fd) {
	ClientList *list = malloc(sizeof(ClientList));
	list->clientId = 0;
	list->roomId = -1;
	list->fd = fd;
	list->username = NULL;
	list->shouldBeRemoved = 0;
	initBuffer(&list->buf);
	return list;
}

void free_ClientList(ClientList *list) {
	assert(list != NULL);
	free(list->username);
	free(list);
}

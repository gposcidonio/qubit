#include "parse.h"

void parseClientCommand(ServerState *state, ClientList *client) {
	printf("%s says: %s", client->username, client->buf.str);
	char *endOfVerb = strchr(client->buf.str, ' ');
	if (endOfVerb == NULL) {
		// send back an error
		sendError(client, INVALID_OPERATION);
		return;
	}

	*endOfVerb = '\0';
	char *verb = client->buf.str;
	if (strcmp(verb, "MSG") == 0) {
		// send message to all clients
		char *message = endOfVerb + 1;
		if (client->roomId != WAITING_ROOM) {
			sendMessageToRoom(
				state,
				client->roomId,
				"ECHO %s %s",
				client->username,
				message
			);
		} else {
			sendError(client, INVALID_OPERATION);
		}
	} else if (strcmp(verb, "CREATER") == 0) {
		char *roomName = endOfVerb + 1;
		endOfVerb = strchr(roomName, ' ');
		if (endOfVerb == NULL) {
			sendError(client, INTERNAL_SERVER_ERROR);
			client->buf.index = 0;
			return;
		}
		*endOfVerb = '\0';
		int roomId = getFreeRoomId(state);

		//lock for room creation
		pthread_mutex_lock(&state->roomMutex);
		if(roomId != -1){
			RoomList *newRoom = new_RoomList(roomId, client->clientId, roomName);
			addRoom(state, newRoom);

			RoomList *oldRoom = getRoomForId(state, client->roomId);
			if(oldRoom != NULL){
				removeIDFromIDList(client->clientId, &oldRoom->clientIds);
			}

			client->roomId = roomId;
			newRoom->clientIds = new_IDList(client->clientId);
			sendMessage(client, "RETAERC %s \r\n", roomName);
		} else {
			sendError(client, MAXIMUM_ROOMS_REACHED);
		}
		pthread_mutex_unlock(&state->roomMutex);
	} else if (strcmp(verb, "CREATEP") == 0) {
		// parse room name
		char *roomName = endOfVerb + 1;
		endOfVerb = strchr(roomName, ' ');
		if (endOfVerb == NULL) {
			sendError(client, INTERNAL_SERVER_ERROR);
			client->buf.index = 0;
			return;
		}
		*endOfVerb = '\0';

		// parse password
		char *password = endOfVerb + 1;
		endOfVerb = strchr(password, ' ');
		if (endOfVerb == NULL) {
			sendError(client, INTERNAL_SERVER_ERROR);
			client->buf.index = 0;
			return;
		}
		*endOfVerb = '\0';

		//lock for room creation
		pthread_mutex_lock(&state->roomMutex);
		int roomId = getFreeRoomId(state);

		if(roomId != -1) {
			RoomList *newRoom = new_RoomList(roomId, client->clientId, roomName);
			newRoom->isPrivate = 1;
			newRoom->password = malloc(strlen(password) + 1);
			strcpy(newRoom->password, password);
			addRoom(state, newRoom);

			client->roomId = roomId;

			IDList *clientIdList = new_IDList(client->clientId);

			newRoom->clientIds = clientIdList;
			sendMessage(client, "PETAERC %s \r\n", roomName);
		} else {
			sendError(client, MAXIMUM_ROOMS_REACHED);
		}
		pthread_mutex_unlock(&state->roomMutex);
	} else if (strcmp(verb, "LISTR") == 0) {
		RoomList *firstRoom = state->rooms;
		RoomList *curRoom = firstRoom;

		if(firstRoom == NULL){
			sendMessage(client, "RTSIL no_rooms -1 \r\n\r\n");
		} else {
			size_t messageSize = (state->numRooms * (MAX_USERNAME_LEN + 4)) + 10;
			char *message = malloc(messageSize);
			memset(message, 0, messageSize);
			strcpy(message, "RTSIL");
			do {
				char *temp = malloc(2 * MAX_USERNAME_LEN);
				memset(temp, 0, 2 * MAX_USERNAME_LEN);
				int privateStatus;
				if(curRoom->isPrivate){
					privateStatus = 2;
				} else {
					privateStatus = 1;
				}
				sprintf(temp, " %s %d %d \r\n", curRoom->roomName, curRoom->roomId, privateStatus);
				strcat(message, temp);
				curRoom = curRoom->next;
				free(temp);
			} while (curRoom != firstRoom);
			strcat(message, "\r\n");
			sendMessage(client, message);
			free(message);
		}
	} else if (strcmp(verb, "LISTU") == 0) {
		RoomList *room = getRoomForId(state, client->roomId);
		if (room == NULL) {
			sendError(client, INVALID_OPERATION);
		} else {
			IDList *curClientId = room->clientIds;

			sendMessage(client, "UTSIL");
			if (curClientId == NULL) {
				sendMessage(client, " \r\n\r\n");
			} else {
				do {
					ClientList *curClient = getClientForId(
						state,
						curClientId->id
					);
					sendMessage(client, " %s \r\n", curClient->username);
					curClientId = curClientId->next;
				} while (curClientId);
				sendMessage(client, "\r\n");
			}
		}
	} else if (strcmp(verb, "JOIN") == 0){
		char *roomIdStr = endOfVerb + 1;
		endOfVerb = strchr(roomIdStr, ' ');
		if (endOfVerb == NULL) {
			sendError(client, INTERNAL_SERVER_ERROR);
			client->buf.index = 0;
			return;
		}
		*endOfVerb = '\0';
		int id = atoi(roomIdStr);
		if(id != 0) {
			RoomList *room = getRoomForId(state, id);
			if (room == NULL) {
				sendError(client, ROOM_DOES_NOT_EXIST);
				client->buf.index = 0;
				return;
			}

			if(!room->isPrivate){
				IDList *newClientId = new_IDList(client->clientId);
				newClientId->next = room->clientIds;
				room->clientIds = newClientId;

				RoomList *oldRoom = getRoomForId(state, client->roomId);
				if(oldRoom != NULL){
					removeIDFromIDList(client->clientId, &oldRoom->clientIds);
				}
				client->roomId = room->roomId;

				sendMessage(client, "NIOJ %i \r\n", id);
			} else {
				sendError(client, INVALID_OPERATION);
			}
		} else {
			sendError(client, INTERNAL_SERVER_ERROR);
			client->buf.index = 0;
		}
	} else if (strcmp(verb, "JOINP") == 0){
		char *roomIdStr = endOfVerb + 1;
		endOfVerb = strchr(roomIdStr, ' ');
		if (endOfVerb == NULL) {
			sendError(client, INTERNAL_SERVER_ERROR);
			client->buf.index = 0;
			return;
		}
		*endOfVerb = '\0';
		int id = atoi(roomIdStr);
		if(id != 0) {
			RoomList *room = getRoomForId(state, id);
			if (room == NULL) {
				sendError(client, ROOM_DOES_NOT_EXIST);
				client->buf.index = 0;
				return;
			}
			if(room->isPrivate){
				char *password = endOfVerb + 1;
				endOfVerb = strchr(password, ' ');
				if (endOfVerb == NULL) {
					sendError(client, INTERNAL_SERVER_ERROR);
					client->buf.index = 0;
					return;
				}
				*endOfVerb = '\0';
				assert(room->password != NULL);
				if(strcmp(password, room->password) == 0){
					IDList *newClientId = new_IDList(client->clientId);
					newClientId->next = room->clientIds;
					room->clientIds = newClientId;

					RoomList *oldRoom = getRoomForId(state, client->roomId);
					if(oldRoom != NULL){
						removeIDFromIDList(client->clientId, &oldRoom->clientIds);
					}

					sendMessage(client, "PNIOJ %i \r\n", id);
				} else {
					sendError(client, INVALID_PASSWORD);
					client->buf.index = 0;
					return;
				}
				client->roomId = room->roomId;
			} else {
				sendError(client, INVALID_OPERATION);
			}
		}
	} else if(strcmp(verb, "LEAVE") == 0) {
		int roomId = client->roomId;
		RoomList *room = getRoomForId(state, roomId);
		if(room != NULL){
			removeIDFromIDList(client->clientId, &room->clientIds);
			sendMessage(client, "EVAEL \r\n");
			sendMessageToRoom(
					state,
					roomId,
					"ECHO server %s has left the room \r\n",
					client->username
					);

			if(room->ownerId == client->clientId){
				if(room->clientIds == NULL){
					removeRoom(state, room);
				} else {
					ClientList *newOwner = getClientForId(state, room->clientIds->id);
					sendMessageToRoom(
							state,
							roomId,
							"ECHO server %s has been promoted to owner \r\n",
							newOwner->username
							);
					room->ownerId = newOwner->clientId;
				}
			}
		} else {
			sendError(client, USER_NOT_PRESENT);
		}
		client->roomId = -1;
	} else if(strcmp(verb, "KICK") == 0){
		RoomList* room = getRoomForId(state, client->roomId);
		if(room == NULL){
			sendError(client, NOT_OWNER);
		} else {
			if(room->ownerId != client->clientId){
				sendError(client, NOT_OWNER);
			} else {
				char *username = endOfVerb + 1;
				endOfVerb = strchr(username, ' ');
				*endOfVerb = '\0';
				ClientList *clientToBeKicked = getClientForUsername(state, username);
				if(clientToBeKicked != NULL){
					clientToBeKicked->roomId = -1;
					removeIDFromIDList(clientToBeKicked->clientId, &room->clientIds);
					sendMessage(client, "KCIK %s \r\n", username);
					sendMessage(clientToBeKicked, "KBYE \r\n");
				} else {
					sendError(client, INVALID_USER);
				}
			}
		}
	} else if(strcmp(verb, "TELL") == 0){
		char *username = endOfVerb + 1;
		endOfVerb = strchr(username, ' ');
		*endOfVerb = '\0';
		ClientList *clientToMessage = getClientForUsername(state, username);
		char *message = endOfVerb + 1;

		if (
			clientToMessage != NULL &&
			client->roomId != WAITING_ROOM &&
			client->roomId == clientToMessage->roomId
		) {
			sendMessage(clientToMessage, "ECHOP %s %s", username, message);
			sendMessage(client, "LLET %s %s \r\n", username, message);
		} else {
			sendError(client, USER_NOT_PRESENT);
		}
	}
	client->buf.index = 0;
}

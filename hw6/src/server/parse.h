#pragma once

#include <string.h>

#include "io.h"
#include "state.h"
#include "../consts.h"
#include "client-list.h"

void parseClientCommand(ServerState *state, ClientList *client);

#include "io.h"

int hasInput(int fd) {
	fd_set readfds, writefds, errorfds;
	struct timeval timeout;
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	FD_ZERO(&errorfds);
	FD_SET(fd, &readfds);
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	return select(
		fd + 1,
		&readfds,
		&writefds,
		&errorfds,
		&timeout
	);
}

int readChar(int fd, Buffer *buf) {
	char c;
	int retVal;
	if ((retVal = read(fd, &c, 1)) != 1) {
		return retVal;
	}

	addChar(c, buf);
	return 1;
}

char *dgetln(int fd) {
	size_t index = 0;
	size_t bufLen = 1024;
	char *line = malloc(sizeof(char) * bufLen);

	do {
		int retVal = read(fd, line + index, 1);
		if (retVal == -1) {
			if (errno != EINTR) {
				continue;
			} else {
				free(line);
				return NULL;
			}
		} else if (retVal == 0) {
			break;
		} else if (retVal == 1) {
			index++;
		}

		if (index == bufLen) {
			char *newLine = malloc(sizeof(char) * 2 * bufLen);
			strncpy(newLine, line, bufLen);
			bufLen *= 2;
			free(line);
		}
	} while (line[index - 1] != '\n');
	line[index] = '\0';

	return line;
}

Command dgetCommand(int fd) {
	Command cmd;
	cmd.verb = NULL;
	cmd.message = NULL;

	char *line = dgetln(fd);
	if (line != NULL) {
		cmd = getCommand(line);
		if (cmd.verb == NULL) {
			free(line);
		}
	}

	return cmd;
}

Command getCommand(char *line) {
	Command cmd;
	cmd.verb = NULL;
	cmd.message = NULL;

	char *verb = line;
	char *eov = strchr(verb, ' ');
	if (eov == NULL) {
		return cmd;
	}

	*eov = '\0';
	char *message = eov + 1;
	char *eom = strstr(message, "\r\n");
	if (eom == NULL) {
		return cmd;
	}

	eom[-1] = '\0';

	cmd.verb = verb;
	cmd.message = message;
	return cmd;
}

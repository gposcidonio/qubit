#include "server.h"

int main(int argc, char **argv) {
	ServerState *state = new_ServerState();
	char *name = *argv;

	// handle flags
	int opt;
	char* maxRoomStr;
	while ((opt = getopt(argc, argv, "ehN")) != -1) {
		switch (opt) {
			case 'e':
				state->echoToStdout = 1;
				break;

			case 'h':
				usage(name);
				quit(state, EXIT_FAILURE);
				break;

			case 'N':
				maxRoomStr = argv[optind];
				for(int i = 0; i < strlen(maxRoomStr); i++){
					if(!isdigit(maxRoomStr[i])){
						usage(name);
						quit(state, EXIT_FAILURE);
					}
				}
				argc--;
				argv++;
				state->maxRooms = atoi(maxRoomStr);
				break;

			default:
				printf("error: bad flag\n");
				return EXIT_FAILURE;
		}
	}
	argc -= optind;
	argv += optind;

	// check argcount
	if (argc != 3) {
		usage(name);
		return EXIT_FAILURE;
	}

	char *config_file_name = argv[2];

	// check if port is valid
	state->port = atoi(argv[0]);
	state->motd = argv[1];
	if (state->port > MAX_PORT) {
		printf("error: port must be between 0 and %i\n", MAX_PORT);
		return EXIT_FAILURE;
	}

	printf("starting server on port %i\n", state->port);

	// open a socket for listening
	if (openListen(state) < 0) {
		printf("server: error starting server\n");
		quit(state, EXIT_FAILURE);
	}

	pthread_t tid;
	pthread_create(&tid, NULL, logDumpJob, state);

	pool_t *loginPool = pool_create_file(config_file_name, NULL);
	if (loginPool == NULL) {
		quit(state, EXIT_FAILURE);
	}

	// listen for connections
	struct sockaddr clientAddr;
	socklen_t clientAddrLen = sizeof(clientAddr);
	while (1) {
		int clientFd = accept(
			state->listenFd,
			&clientAddr,
			&clientAddrLen
		);

		if (clientFd == -1) {
			perror("server");
			quit(state, EXIT_FAILURE);
		}

		// start the login thread
		LoginStruct *loginStruct = malloc(sizeof(LoginStruct));
		loginStruct->state = state;
		loginStruct->clientFd = clientFd;
		pool_queue(loginPool, login, loginStruct);
	}

	printf("server ending\n");
	quit(state, EXIT_SUCCESS);

	// should never get here
	return EXIT_FAILURE;
}

int openListen(ServerState *state) {
	// open socket
	state->listenFd = socket(AF_INET, SOCK_STREAM, 6);
	if (state->listenFd < 0) {
		perror("server");
		return -1;
	}

	// configure socket to listen for connections to any local ip
	struct sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(state->port);
	if (
		bind(
			state->listenFd,
			(struct sockaddr *)&serverAddr,
			sizeof(serverAddr)
		) < 0
	) {
		perror("server");
		return -1;
	}

	// start the socket listening for connections
	if (listen(state->listenFd, 128) < 0) {
		perror("server");
		return -1;
	}

	return 0;
}

void* logDumpJob(void *vargp){
	ServerState *state = (ServerState *)vargp;
	char* line;
	size_t size;
	while(1){
		if(getline(&line, &size, stdin) != -1){
			if(strcmp(line, "/logdump\n") == 0){
				logDump(state);
			}
		}
	}
	return NULL;
}

void logDump(ServerState *state){
	pthread_mutex_lock(&state->logFileMutex);
	FILE *fp = fopen("auth.log", "r");
	if(fp == NULL){
		printf("auth.log does not exist.\n");
		return;
	}
	int c;
	while((c = getc(fp)) != EOF){
		putchar(c);
	}
	fclose(fp);
	pthread_mutex_unlock(&state->logFileMutex);
}

void usage(char *name) {
	printf(
		"%s -[eh] PORT_NUMBER MOTD CONFIG_FILE\n"
		"-e          Echo messages received on server's stdout\n"
		"-h          Displays help menu and returns EXIT_SUCCESS\n"
		"PORT_NUMBER Port number to listen on.\n"
		"MOTD        Message to display to the client when they connect\n"
		"CONFIG_FILE Text file with thread pool values stored in it.",
		name
	);
}

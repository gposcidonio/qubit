#pragma once

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>

#include "consts.h"
#include "buffer.h"

typedef struct {
	char *verb;
	char *message;
} Command;

int hasInput(int fd);
int readChar(int fd, Buffer *buf);
char *dgetln(int fd);
Command dgetCommand(int fd);
Command getCommand(char *line);

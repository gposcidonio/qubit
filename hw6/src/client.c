#include "client.h"

int main(int argc, char **argv) {
	ClientState *state = new_ClientState();
	char *binName = *argv;

	// handle flags
	int opt;
	while ((opt = getopt(argc, argv, "hc")) != -1) {
		switch (opt) {
			case 'h':
				usage(binName);
				quit(state, EXIT_SUCCESS);
				break;

			case 'c':
				state->newUser = 1;
				break;

			default:
				usage(binName);
				quit(state, EXIT_FAILURE);
		}
	}
	argc -= optind;
	argv += optind;

	// check arg format
	if (argc < 3) {
		usage(binName);
		quit(state, EXIT_FAILURE);
	}
	state->username   = argv[0];
	state->serverIp   = argv[1];
	state->serverPort = argv[2];

	// open connection to server
	if (connectToServer(state, state->serverIp, state->serverPort) < 0) {
		fprintf(stderr, "client: error occurred while connecting\n");
		quit(state, EXIT_FAILURE);
	}

	// authenticate with server
	if (authUser(state, state->username) != 0) {
		printf("client: error occurred while authing\n");
		quit(state, EXIT_FAILURE);
	}

	pthread_t listUserThreadId;
	pthread_create(&listUserThreadId, NULL, &refreshUserList, (void *)state);

	initIo(state);

	int running = 1;
	while (running) {
		// check socket for new messages
		if (hasInput(state->serverFd) > 0) {
			Buffer *buf = &state->serverBuf;
			if (readChar(state->serverFd, buf) == 0) {
				if (buf->index > 0) {
					addChar('\0', buf);
					parseServerCommand(state, *buf);
				}
				break;
			}

			if(strncmp(buf->str, "UTSIL", 5) == 0 || strncmp(buf->str, "RTSIL", 5) == 0){
				if (strcmp(&buf->str[buf->index - 4], "\r\n\r\n") == 0) {
					addChar('\0', buf);
					parseServerCommand(state, *buf);
					memset(buf->str, 0, buf->len);
					buf->index = 0;
				}
			} else {
				// if we read the whole line, run it
				if (buf->str[buf->index - 1] == '\n') {
					addChar('\0', buf);
					parseServerCommand(state, *buf);
					memset(buf->str, 0, buf->len);
					buf->index = 0;
				}
			}
		}

		// check stdin for new input
		if (hasInput(STDIN_FILENO)) {
			Buffer *buf = &state->stdinBuf;
			if (readChar(STDIN_FILENO, buf) == 0) {
				break;
			}

			char c = buf->str[buf->index - 1];
			if (c == 0x7f) {
				// handle backspace character
				if (buf->index < 2) {
					buf->index = 0;
				} else {
					buf->index -= 2;
				}
			} else if (c == '\n' || c == '\r') {
				// if we read the whole line, send it
				buf->str[buf->index - 1] = '\0';
				parseClientCommand(state, *buf);
				buf->index = 0;
			}

			display(state);
		}

		struct timespec sleepTime;
		sleepTime.tv_sec = 0;
		sleepTime.tv_nsec = 20000;
		nanosleep(&sleepTime, NULL);
	}

	quit(state, EXIT_SUCCESS);

	// execution should not get past the quit
	assert(0);
	return EXIT_SUCCESS;
}

int connectToServer(ClientState *state, char *ip, char *port) {
	// open socket
	state->serverFd = socket(AF_INET, SOCK_STREAM, 6);
	if (state->serverFd < 0) {
		perror("client");
		return -1;
	}

	// connect socket to remote server
	struct addrinfo *res = NULL;
	int retVal = getaddrinfo(ip, port, NULL, &res);
	if (retVal != 0) {
		close(state->serverFd);
		state->serverFd = -1;
		printf("client: %s\n", gai_strerror(retVal));
		return -1;
	}

	if (res == NULL) {
		// couldn't find a server
		close(state->serverFd);
		state->serverFd = -1;
		printf("client: could not find server\n");
		return -1;
	}

	if (connect(state->serverFd, res->ai_addr, res->ai_addrlen) < 0) {
		perror("client");
		return -1;
	}

	freeaddrinfo(res);

	return 0;
}

void usage(char *binName) {
	printf(
		"%s [-h] NAME SERVER_IP SERVER_PORT\n"
		"-h          Displays help menu and returns EXIT_SUCCESS\n"
		"-c          Requests to server to create a new user\n"
		"NAME        Username to display when chatting.\n"
		"SERVER_IP   IP address of server to connect to.\n"
		"SERVER_PORT Port to connect to.\n",
		binName
	);
}

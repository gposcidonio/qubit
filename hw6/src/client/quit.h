#pragma once

#include <ncurses.h>

#include "state.h"

void quit(ClientState *state, int exitStatus);

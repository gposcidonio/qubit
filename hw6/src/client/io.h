#pragma once

#include <ncurses.h>

#include "state.h"

void initIo(ClientState *state);
void displayMessage(ClientState *state, char *message, ...);
void displayColoredMessage(ClientState *state, char *color, char *message, ...);
void display(ClientState *state);
void displayUserList(ClientState *state, char **users, int numUsers);

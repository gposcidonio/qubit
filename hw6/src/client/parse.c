#include "parse.h"

void parseServerCommand(ClientState *state, Buffer buf) {
	Command cmd;
	if(strncmp(buf.str, "UTSIL", 5) == 0){
		cmd.verb = "UTSIL";
	} else if(strncmp(buf.str, "RTSIL", 5) == 0){
		cmd.verb = "RTSIL";
	} else {
		cmd = getCommand(buf.str);
		if (cmd.verb == NULL) {
			return;
		}
	}

	if (strcmp(cmd.verb, "ECHO") == 0) {
		char *usern = cmd.message;
		char *endOfUsern = strchr(usern, ' ');
		if (endOfUsern == NULL) {
			return;
		}

		*endOfUsern = '\0';
		char *message = endOfUsern + 1;

		if(strcmp(usern, "server") == 0){
			displayColoredMessage(state, "cyan", "%s > %s\n", usern, message);
		} else {
			displayMessage(state, "%s > %s\n", usern, message);
		}
	} else if(strcmp(cmd.verb, "ECHOP") == 0){
		char *usern = cmd.message;
		char *endOfUsern = strchr(usern, ' ');
		if (endOfUsern == NULL) {
			return;
		}

		*endOfUsern = '\0';
		char *message = endOfUsern + 1;

		displayColoredMessage(state, "yellow", "%s > %s\n", usern, message);
	} else if(strcmp(cmd.verb, "PNIOJ") == 0){
		char* roomIdStr = cmd.message;
		displayColoredMessage(state, "blue", "Successfully joined private room %s.\n", roomIdStr);
	} else if(strcmp(cmd.verb, "NIOJ") == 0){
		char* roomIdStr = cmd.message;
		displayColoredMessage(state, "blue", "Successfully joined public room %s.\n", roomIdStr);
	} else if(strcmp(cmd.verb, "PETAERC") == 0){
		char* roomNameStr = cmd.message;
		displayColoredMessage(state, "blue", "Successfully created private room %s.\n", roomNameStr);
	} else if(strcmp(cmd.verb, "RETAERC") == 0){
		char* roomNameStr = cmd.message;
		displayColoredMessage(state, "blue", "Successfully created public room %s.\n", roomNameStr);
	} else if(strcmp(cmd.verb, "LLET") == 0){
		char *usern = cmd.message;
		char *endOfUsern = strchr(usern, ' ');
		if (endOfUsern == NULL) {
			return;
		}

		*endOfUsern = '\0';
		char *message = endOfUsern + 1;
		displayColoredMessage(state, "yellow", "To %s > %s\n", usern, message);
	} else if(strcmp(cmd.verb, "EVAEL") == 0){
		displayColoredMessage(state, "blue", "You have left the chat room.\n");
	} else if(strcmp(cmd.verb, "KBYE") == 0){
		displayColoredMessage(state, "blue", "You have been kicked from the chat room.\n");
	} else if(strcmp(cmd.verb, "KCIK") == 0){
		char* username = cmd.message;
		displayColoredMessage(state, "blue", "You have kicked %s from the chat room.\n", username);
	} else if (strcmp(cmd.verb, "ERR") == 0) {
		displayColoredMessage(state, "red", "%s : %s\n", cmd.verb, cmd.message);
	} else if (strcmp(cmd.verb, "UTSIL") == 0) {
		char *usernameList[50];
		int numUsers = 0;
		char* message = buf.str;
		char* nextSpace = strchr(message, ' ');
		*nextSpace = '\0';
		message = nextSpace + 1;
		char* username = message;
		nextSpace = strchr(message, ' ');
		while(nextSpace != NULL && numUsers < 50){
			*nextSpace = '\0';
			if(strncmp(username, "\r\n", 2) != 0){
				usernameList[numUsers] = malloc(strlen(username) + 1);
				strcpy(usernameList[numUsers], username);
				numUsers++;
			}
			username = nextSpace + 1;
			nextSpace = strchr(username, ' ');
		}
		displayUserList(state, usernameList, numUsers);
		for(int i = 0; i < numUsers; i++){
			free(usernameList[i]);
		}
	} else if(strcmp(cmd.verb, "RTSIL") == 0) {
		typedef struct Room {
			char *roomName;
			int roomId;
			int isPrivate;
		} Room;

		Room *roomArray[100];

		int numRooms = 0;
		char *message = buf.str;
		char *nextSpace = strchr(message, ' ');
		if(nextSpace == NULL) return;
		*nextSpace = '\0';
		message = nextSpace + 1;

		char *endOfMessage = strstr(message, "\r\n\r\n");
		char *endOfRoomStr = strstr(message, "\r\n");
		do {
			roomArray[numRooms] = malloc(sizeof(Room));
			roomArray[numRooms]->roomName = malloc(30);

			nextSpace = strchr(message, ' ');
			if(nextSpace == NULL) return;
			*nextSpace = '\0';
			strcpy(roomArray[numRooms]->roomName, message);
			message = nextSpace + 1;

			nextSpace = strchr(message, ' ');
			if(nextSpace == NULL) return;
			*nextSpace = '\0';
			roomArray[numRooms]->roomId = atoi(message);
			message = nextSpace + 1;

			nextSpace = strchr(message, ' ');
			if(nextSpace == NULL) return;
			*nextSpace = '\0';
			roomArray[numRooms]->isPrivate = atoi(message) == 2 ? 1 : 0;
			message = nextSpace + 4;

			numRooms++;

			if(endOfMessage == endOfRoomStr) break;

			endOfRoomStr = strstr(message, "\r\n");
		} while (numRooms < 100);

		displayMessage(state, "Listing rooms...\n");
		for(int i = 0; i < numRooms; i++){
			char* privacy = roomArray[i]->isPrivate ? "Private" : "Public";
			if(roomArray[i]->isPrivate){
				displayColoredMessage(
						state,
						"purple",
						"Room %d : %s - %s\n",
						roomArray[i]->roomId,
						roomArray[i]->roomName,
						privacy
				);
			} else {
				displayMessage(
						state,
						"Room %d : %s - %s\n",
						roomArray[i]->roomId,
						roomArray[i]->roomName,
						privacy
				);
			}
			free(roomArray[i]->roomName);
			free(roomArray[i]);
		}
	} else {
		displayMessage(
			state,
			"server says: %s %s\n",
			cmd.verb,
			cmd.message
		);
	}
}

void parseClientCommand(ClientState *state, Buffer buf) {
	char *command = buf.str;
	if (buf.index > 1002) {
		command = malloc(sizeof(char) * 1001);
		memcpy(command, buf.str, 1000);
		command[1000] = '\0';
	}

	if (*command == '/'){
		char* body = strchr(command, ' ');
		if (body != NULL){
			*body = '\0';
			body++;
		}

		if (strcmp(command, "/help") == 0) {
			displayMessage(state, "Commands:\n");
			displayMessage(state, "\t/help\n");
			displayMessage(state, "\t/creater\n");
			displayMessage(state, "\t/createp\n");
			displayMessage(state, "\t/listusers\n");
			displayMessage(state, "\t/listrooms\n");
			displayMessage(state, "\t/join\n");
			displayMessage(state, "\t/joinp\n");
			displayMessage(state, "\t/leave\n");
			displayMessage(state, "\t/kick\n");
			displayMessage(state, "\t/tell\n");
		} else if (strcmp(command, "/creater") == 0){
			dprintf(state->serverFd, "CREATER %s \r\n", body);
			dprintf(state->serverFd, "LISTU \r\n");
			state->inRoom = 1;
		} else if(strcmp(command, "/createp") == 0){
			dprintf(state->serverFd, "CREATEP %s \r\n", body);
			dprintf(state->serverFd, "LISTU \r\n");
			state->inRoom = 1;
		} else if(strcmp(command, "/listusers") == 0){
			dprintf(state->serverFd, "LISTU \r\n");
		} else if(strcmp(command, "/listrooms") == 0){
			dprintf(state->serverFd, "LISTR \r\n");
		} else if (strcmp(command, "/join") == 0){
			dprintf(state->serverFd, "JOIN %s \r\n", body);
			dprintf(state->serverFd, "LISTU \r\n");
			state->inRoom = 1;
		} else if(strcmp(command, "/joinp") == 0){
			dprintf(state->serverFd, "JOINP %s \r\n", body);
			dprintf(state->serverFd, "LISTU \r\n");
			state->inRoom = 1;
		} else if(strcmp(command, "/leave") == 0){
			dprintf(state->serverFd, "LEAVE \r\n");
			state->inRoom = 0;
		} else if (strcmp(command, "/kick") == 0) {
			dprintf(state->serverFd, "KICK %s \r\n", body);
		} else if (strcmp(command, "/tell") == 0) {
			dprintf(state->serverFd, "TELL %s \r\n", body);
		} else {
			printf("Unknown command: %s \n", command);
		}
	} else {
		dprintf(state->serverFd, "MSG %s \r\n", command);
	}
}

void *refreshUserList(void *vargp){
	ClientState *state = vargp;
	while(1){
		if (state->inRoom) {
			dprintf(state->serverFd, "LISTU \r\n");
		}
		sleep(15);
	}
	return NULL;
}

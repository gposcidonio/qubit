#pragma once

#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include "../buffer.h"

typedef struct {
	int serverFd;
	int newUser;
	char *username;
	char *serverIp;
	char *serverPort;
	Buffer serverBuf;
	Buffer stdinBuf;
	WINDOW *textWindow;
	WINDOW *inputWindow;
	WINDOW *userListWindow;
	int inRoom;
} ClientState;

ClientState *new_ClientState();
void free_ClientState(ClientState *state);

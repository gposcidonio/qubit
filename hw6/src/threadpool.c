#include "threadpool.h"

job_t *new_job(job_t *next, void *(job_func)(void *), void *job_arg) {
	job_t *job = malloc(sizeof(job_t));
	job->job_next = next;
	job->job_func = job_func;
	job->job_arg = job_arg;

	return job;
}

void free_job(job_t *job) {
	free(job);
}

int isNum(char *line) {
	while (*line && *line != '\n') {
		if (*line < '0' || *line > '9') {
			return 0;
		}
		line++;
	}

	return 1;
}

pool_t *pool_create_file(char *fn, pthread_attr_t *attr) {
	FILE *fp = fopen(fn, "r");
	if (fp == NULL) {
		fprintf(stderr, "file does not exist\n");
		return NULL;
	}

	char *line;
	size_t line_cap;

	// read min pool size
	if (getline(&line, &line_cap, fp) == -1) {
		fprintf(stderr, "bad config file format\n");
		return NULL;
	}
	uint16_t min_size;
	if (!isNum(line)) {
		fprintf(stderr, "bad min pool size in config file\n");
		return NULL;
	}
	min_size = atoi(line);
	if (min_size < 1) {
		fprintf(stderr, "min size is too small\n");
		return NULL;
	}

	// read max pool size
	if (getline(&line, &line_cap, fp) == -1) {
		fprintf(stderr, "bad config file format\n");
		return NULL;
	}
	uint16_t max_size;
	if (!isNum(line)) {
		fprintf(stderr, "bad max pool size in config file\n");
		return NULL;
	}
	max_size = atoi(line);
	if (max_size < min_size) {
		fprintf(stderr, "max size must be greater than min size\n");
		return NULL;
	}

	// read queue size
	if (getline(&line, &line_cap, fp) == -1) {
		fprintf(stderr, "bad config file format\n");
		return NULL;
	}
	size_t queue_size;
	if (!isNum(line)) {
		fprintf(stderr, "bad queue size in config file\n");
		return NULL;
	}
	queue_size = atol(line);

	// read linger time
	if (getline(&line, &line_cap, fp) == -1) {
		fprintf(stderr, "bad config file format\n");
		return NULL;
	}
	uint16_t linger_time;
	if (!isNum(line)) {
		fprintf(stderr, "bad linger time in config file\n");
		return NULL;
	}
	int x = atoi(line);
	if (x < 0) {
		fprintf(stderr, "bad linger time in config file\n");
	}
	linger_time = x;

	if (getline(&line, &line_cap, fp) != -1) {
		fprintf(stderr, "bad config file format\n");
	}

	return pool_create(min_size, max_size, linger_time, attr);
}

pool_t *pool_create(
	uint16_t min,
	uint16_t max,
	uint16_t linger,
	pthread_attr_t* attr
) {
	assert(max > min);

	pool_t *pool = malloc(sizeof(pool_t));

	// initialize pool values
	pool->pool_forw = pool;
	pool->pool_back = pool;

	// create mutexes, conditions, etc.
	if (pthread_mutex_init(&pool->pool_mutex, NULL) != 0) {
		return NULL;
	}

	if (pthread_cond_init(&pool->pool_busycv, NULL) != 0) {
		return NULL;
	}

	if (pthread_cond_init(&pool->pool_workcv, NULL) != 0) {
		return NULL;
	}

	if (pthread_cond_init(&pool->pool_waitcv, NULL) != 0) {
		return NULL;
	}

	if (attr == NULL) {
		// use default pthread attrs
		if (pthread_attr_init(&pool->pool_attr) != 0) {
			return NULL;
		}
	} else {
		pool->pool_attr = *attr;
	}

	pool->pool_active = NULL;
	pool->pool_head = NULL;
	pool->pool_tail = NULL;

	pool->pool_flags = 0;
	pool->pool_linger = linger;
	pool->pool_minimum = min;
	pool->pool_maximum = max;
	pool->pool_nthreads = 0;
	pool->pool_idle = 0;

	// start up workers
	int i;
	for (i = 0; i < pool->pool_minimum; i++) {
		pthread_t thread;
		pthread_create(&thread, &pool->pool_attr, pool_worker_func, pool);
		pool->pool_nthreads++;
	}

	return pool;
}

void free_pool(pool_t *pool) {
	pthread_mutex_destroy(&pool->pool_mutex);
	pthread_cond_destroy(&pool->pool_busycv);
	pthread_cond_destroy(&pool->pool_workcv);
	pthread_cond_destroy(&pool->pool_waitcv);
	pthread_attr_destroy(&pool->pool_attr);
	free(pool);
}

int pool_queue(pool_t *pool, void *(*func)(void *), void *arg) {
	// get locked access to pool
	if (pthread_mutex_lock(&pool->pool_mutex) != 0) {
		perror("error queueing job");
		return -1;
	}

	// add job to queue
	job_t *job = new_job(pool->pool_head, func, arg);
	pool->pool_head = job;
	if (pool->pool_tail == NULL) {
		pool->pool_tail = job;
	}

	printf("n idle things: %i\n", pool->pool_idle);
	if (pool->pool_idle > 0) {
		// signal an idle worker to work
		pthread_cond_signal(&pool->pool_workcv);
	} else if (pool->pool_nthreads < pool->pool_maximum) {
		printf("spawning new thread\n");
		pthread_t thread;
		pthread_create(&thread, &pool->pool_attr, pool_worker_func, pool);
		pool->pool_nthreads++;
	} else {
		// do nothing
		// a worker thread will pick up the job when it can
	}

	// unlock pool
	if (pthread_mutex_unlock(&pool->pool_mutex) != 0) {
		perror("error queueing job");
		return -1;
	}

	return 0;
}

void pool_wait(pool_t *pool) {
	if (pthread_mutex_lock(&pool->pool_mutex) != 0) {
		return;
	}

	pool_wait_locked(pool);

	if (pthread_mutex_unlock(&pool->pool_mutex) != 0) {
		return;
	}
}

void pool_wait_locked(pool_t *pool) {
	if (pool->pool_flags != 0) {
		return;
	}

	pool->pool_flags = POOL_WAIT;
	pthread_cond_wait(&pool->pool_waitcv, &pool->pool_mutex);
	pool->pool_flags = 0;
}

void pool_destroy(pool_t *pool) {
	if (pthread_mutex_lock(&pool->pool_mutex) != 0) {
		return;
	}

	if (pool->pool_flags == 0) {
		pthread_mutex_unlock(&pool->pool_mutex);
		return;
	}

	pool->pool_flags = POOL_DESTROY;

	// clear the job queue
	if (pool->pool_head != NULL) {
		job_t *job = pool->pool_head;
		job_t *nextJob;

		do {
			nextJob = job->job_next;
			free_job(job);
			job = nextJob;
		} while (job != NULL);
	}

	// wait for jobs to terminate
	pool_wait_locked(pool);

	free_pool(pool);
}

void *pool_worker_func(void *arg) {
	pool_t *pool = (pool_t *)arg;
	time_t lastJobExec = time(NULL);

	while (1) {
		if (pthread_mutex_lock(&pool->pool_mutex) != 0) {
			printf("thread exiting\n");
			return NULL;
		}

		// check linger time
		if (time(NULL) - lastJobExec > pool->pool_linger) {
			// terminate
			pool->pool_nthreads--;
			if (pthread_mutex_unlock(&pool->pool_mutex) != 0) {
				printf("thread exiting\n");
				return NULL;
			}
		}

		// check for jobs
		if (pool->pool_tail != NULL) {
			// if there's a job
			job_t *job = getNextJob(pool);

			// run the job
			if (pthread_mutex_unlock(&pool->pool_mutex) != 0) {
				printf("thread exiting\n");
				return NULL;
			}
			job->job_func(job->job_arg);
			time(&lastJobExec);
			free_job(job);
		} else if (pool->pool_nthreads > pool->pool_minimum) {
			// if there's no job and we're extraneous, terminate
			pool->pool_nthreads--;
			if (pthread_mutex_unlock(&pool->pool_mutex) != 0) {
				printf("thread exiting\n");
				return NULL;
			}
			break;
		} else {
			// we're idle now
			pool->pool_idle++;

			// if we were the last running thread, broadcast
			if (pool->pool_idle == pool->pool_nthreads) {
				pthread_cond_broadcast(&pool->pool_waitcv);
			}

			// if the pool is in destroy mode, exit
			if (pool->pool_flags == POOL_DESTROY) {
				printf("thread exiting\n");
				return NULL;
			}

			// wait for a new job
			pthread_cond_wait(&pool->pool_workcv, &pool->pool_mutex);
			pool->pool_idle--;
			job_t *job = getNextJob(pool);

			// run the job
			if (pthread_mutex_unlock(&pool->pool_mutex) != 0) {
				printf("thread exiting\n");
				return NULL;
			}
			job->job_func(job->job_arg);
			time(&lastJobExec);
			free_job(job);
		}
	}

	printf("thread exiting\n");
	return NULL;
}

job_t *getNextJob(pool_t *pool) {
	// if there's a job, run the job
	job_t *job = pool->pool_tail;

	// update the tail
	job_t *newTail = pool->pool_head;
	if (newTail == job) {
		pool->pool_tail = NULL;
		pool->pool_head = NULL;
	} else {
		assert(newTail != NULL);
		while (newTail->job_next->job_next != NULL) {
			newTail = newTail->job_next;
		}
		newTail->job_next = NULL;
		pool->pool_tail = newTail;
	}

	return job;
}

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "exec-mode.h"

typedef enum {
	RUNNING,
	STOPPED
} JobStatus;

typedef struct Job {
	int jid;
	pid_t pid;
	char* name;
	struct Job* next;
	struct Job* prev;
	ExecutionMode mode;
	JobStatus status;
	pid_t pgrp;
} Job;

Job *new_Job(int pid, char* name, ExecutionMode mode);
void free_Job(Job *job);

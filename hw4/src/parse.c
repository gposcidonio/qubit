#include "parse.h"

TokenList *parse(ShellState *state, char *line) {
	TokenList *list, *curToken;
	char c;
	int i = 0, lineIndex = 0;

	list = new_TokenList();
	curToken = list;

	if (line[0] == '#') {
		if (state->mode == SHELL) putc('\n', stdout);
		return NULL;
	}

	// parse the line into tokens
	while ((c = line[lineIndex++])) {
		if (c == EOF) {
			// if this is the first char of the line, output "exit"
			if (list == curToken && i == 0) {
				strcpy(list->data, "exit");
				i += 4;
			}

			break;
		}

		if (c == ' ' && i != 0) {
			// if we hit the end of a token:
			// terminate the token
			curToken->data[i] = '\0';

			// start the next
			TokenList *next = new_TokenList();
			curToken->next = next;
			curToken = next;
			i = 0;
		} else if (c == '|' || c == '>' || c == '<' || c == '&') {
			TokenList *next;

			// if we were in a token (and not the redirection fd)
			curToken->data[i] = '\0';
			if (i != 0 && (c == '&' || !isNumber(curToken->data))) {
				// start the next one
				next = new_TokenList();
				curToken->next = next;
				curToken = next;
				i = 0;
			}

			// insert a redirection token
			if (c == '|') curToken->type = PIPE;
			else if (c == '>') curToken->type = FILE_OUT;
			else if (c == '<') curToken->type = FILE_IN;
			else if(c == '&') curToken->type = AMPERSAND;
			curToken->data[i] = '\0';

			// start the next token
			next = new_TokenList();
			curToken->next = next;
			curToken = next;
			i = 0;
		} else if (c != ' ') {
			// if this token is too big
			if (i >= curToken->width) {
				// increase the container size
				size_t newWidth = 2 * curToken->width;
				char *newToken = malloc(sizeof(char) * newWidth);
				memcpy(
					newToken,
					curToken->data,
					sizeof(char) * curToken->width
				);

				free(curToken->data);
				curToken->data = newToken;
				curToken->width = newWidth;
			}

			// write the char to the token
			curToken->data[i++] = c;
		}
	}
	if (state->mode == SHELL) putc('\n', stdout);
	curToken->data[i] = '\0';

	if (state->mode == SCRIPT && list->data[0] == '#') {
		free_TokenList(list);
		return NULL;
	}

	// if we have a dangling blank token
	if (i == 0 && list->next != NULL) {
		TokenList *token = list;

		// find and free the dangler
		while (token->next->next != NULL) token = token->next;
		free_TokenList(token->next);
		token->next = NULL;
	}


	//Now here we replace any variables in the token list
	//if they exist.
	TokenList *tmp = list;
	while((tmp = tmp->next) != NULL){
		if(*(tmp->data) == '$'){
			// the arg is a variable so get its
			// value and replace it in the token
			char *value;
			if((value = getenv(tmp->data + 1)) != NULL){
				free(tmp->data);
				tmp->data = malloc(strlen(value));
				strcpy(tmp->data, value);
			}
		}
	}

	return list;
}

int isNumber(char *str) {
	while (*str) {
		if (*str < '0' || '9' < *str) {
			return 0;
		}
		str++;
	}

	return 1;
}

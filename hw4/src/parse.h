#pragma once

#include <string.h>
#include <stdio.h>

#include "io.h"
#include "token.h"
#include "state.h"

TokenList *parse(ShellState *state, char *line);
int isNumber(char *str);

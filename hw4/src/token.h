#pragma once

#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
	STANDARD,
	PIPE,
	FILE_IN,
	FILE_OUT,
	AMPERSAND
} TokenType;

typedef struct TokenList {
	char *data;
	size_t width;
	TokenType type;
	struct TokenList *next;
} TokenList;

TokenList *new_TokenList(void);
TokenList *new_TokenListw(size_t width);
TokenList *new_TokenListd(char *data);
void free_TokenList(TokenList *tokenList);

#include "quit.h"

void quit(ShellState *state) {
	// clean up and free
	writeHistory(state);
	tcsetattr(STDIN_FILENO, TCSANOW, &state->oldTio);
	free_ShellState(state);

	exit(EXIT_SUCCESS);
}

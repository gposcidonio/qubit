#include "command.h"

CommandList *new_CommandList(TokenList *tokens) {
	CommandList *command = malloc(sizeof(CommandList));
	command->tokens = tokens;
	command->mode = FOREGROUND;
	command->next = NULL;
	command->fds[0] = 0;
	command->fds[1] = 1;
	command->fds[2] = 2;
	return command;
}

void free_CommandList(CommandList *list) {
	CommandList *next = list->next;
	if (list->tokens != NULL) free_TokenList(list->tokens);
	free(list);
	if (next != NULL) free_CommandList(next);
}

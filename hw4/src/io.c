#include "io.h"

char *readline(ShellState *state) {
	size_t bufSize = 128;
	char *buffer = malloc(sizeof(char) * bufSize);
	int c, prevC, index = 0;
	int escape = 0;
	HistoryEntry *curHistoryEntry = NULL;

	while ((c = fgetc(state->inputFile)) != '\n') {
		if (index >= bufSize) {
			// if there are too many chars for the buffer, double it
			char *tmp = malloc(sizeof(char) * 2 * bufSize);
			memcpy(tmp, buffer, sizeof(char) * bufSize);
			free(buffer);
			buffer = tmp;
		}

		if (escape == 1 && c == '[') {
			escape = 2;
			continue;
		} else if (escape == 2) {
			// we've read CSI [
			// c is the control character
			if (c == 'A') { // up
				// select previous entry
				if (curHistoryEntry == NULL) {
					curHistoryEntry = state->historyTail;
				} else {
					curHistoryEntry = curHistoryEntry->prev;
				}
			} else if (c == 'B') { // down
				// select next entry
				if (curHistoryEntry == NULL) {
					curHistoryEntry = state->history;
				} else {
					curHistoryEntry = curHistoryEntry->next;
				}
			}

			if (c == 'A' || c == 'B') {
				// clear line
				if (index) printf("[%iD[K", index);

				if (curHistoryEntry == NULL) {
					index = 0;
				} else {
					// copy entry into buffer
					free(buffer);
					size_t len = strlen(curHistoryEntry->text);
					buffer = malloc(sizeof(char) * (len + 1));
					strcpy(buffer, curHistoryEntry->text);
					index = len;

					// redisplay
					printf("%s", buffer);
				}
			}

			escape = 0;
			continue;
		} else {
			if (c == 127 || c == '\b') {
				// backspace
				if (index > 0) {
					index--;
					printf("[D [D");
				}
			} else if (c == EOF || c == 4) {
				// EOF / end of stream
				if (index == 0) {
					buffer[0] = EOF;
					index = 1;
					break;
				} else if (state->mode == SCRIPT) {
					break;
				}
			} else if (c == 27) {
				// ESC char
				escape = 1;
			} else if (c == 12) {
				// clear
				printf("[2J[0;0f");
				printPrompt();
				buffer[index] = '\0';
				printf("%s", buffer);
			} else if (c == '\t') {
				buffer[index] = '\0';
				if (prevC == '\t') {
					index += tabComplete(
						state,
						&buffer,
						index,
						bufSize
					);
					while (index > bufSize) bufSize *= 2;
				}
			} else {
				buffer[index++] = c;
				if (state->mode == SHELL) putc(c, stdout);
			}
		}

		prevC = c;
	}
	buffer[index] = '\0';

	return buffer;
}

size_t tabComplete(
	ShellState *state,
	char **buf,
	size_t bufIndex,
	size_t bufSize
) {
	size_t startingIndex = bufIndex;
	char *originalPath = getenv("PATH");
	char *path = malloc(strlen(originalPath) + 1);
	char *savedPath = path;
	strcpy(path, originalPath);

	TrieNode *trie = new_TrieNode();

	// put programs in path into trie
	char *pathElem;
	while ((pathElem = strsep(&path, ":")) != NULL) {
		DIR *dir = opendir(pathElem);
		if (dir == NULL) continue;

		struct dirent *entry;
		while ((entry = readdir(dir)) != NULL) {
			if (entry->d_type != DT_REG && entry->d_type != DT_LNK) {
				continue;
			}
			addStringToTrie(trie, entry->d_name);
		}

		closedir(dir);
	}
	free(savedPath);

	// get the subtrie restricted by the buffer
	TrieNode *restrictedTrie = trie;
	int i;
	for (i = 0; (*buf)[i] && restrictedTrie != NULL; i++) {
		restrictedTrie = restrictedTrie->children[(int)(*buf)[i]];
	}

	// if we have some results
	if (restrictedTrie != NULL) {
		if (restrictedTrie->numChildren > 1) {
			StringList *strings = getStringsInTrie(restrictedTrie);
			StringList *tmp;
			putc('\n', stdout);
			for (tmp = strings; tmp != NULL; tmp = tmp->next) {
				int j;
				for (j = 0; j < i; j++) putc((*buf)[j], stdout);
				printf("%s\n", tmp->str);
			}
			printPrompt();
			printf("%s", *buf);
		} else {
			while (
				restrictedTrie->numChildren == 1 &&
				!restrictedTrie->stringExists
			) {
				// go to the child
				TrieNode *nextLevel = NULL;
				for (i = 0; i < TRIE_NCHILDREN && nextLevel == NULL; i++) {
					if (restrictedTrie->children[i] != NULL) {
						nextLevel = restrictedTrie->children[i];
						break;
					}
				}

				// if the buf is too small, embiggen it
				if (bufIndex == bufSize) {
					char *newBuf = malloc(sizeof(char) * bufSize << 1);
					strncpy(newBuf, *buf, bufSize);
					bufSize <<= 1;
					free(*buf);
					*buf = newBuf;
				}

				(*buf)[bufIndex++] = i;
				restrictedTrie = nextLevel;
				printf("%c", i);
			}
		}
	}

	free_TrieNode(trie);
	free(path);
	// if we didn't match, sound the bell
	if (bufIndex == startingIndex) printf("\a");

	return bufIndex - startingIndex;
}

#pragma once

#include <termios.h>

#include "exec.h"
#include "quit.h"
#include "parse.h"
#include "state.h"
#include "prompt.h"
#include "io.h"
#include "global-state.h"

void signalHandler(int signal);
int main(int argc, char **argv, char **envp);

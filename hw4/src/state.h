#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>

#include "history-entry.h"
#include "job.h"

typedef enum {
	SHELL,
	SCRIPT
} ShellMode;

typedef struct {
	int debug;
	int timeCounting;
	ShellMode mode;
	FILE *inputFile;
	char *prevDir;
	char **envVars;
	HistoryEntry *history;
	HistoryEntry *historyTail;
	Job *jobList;
	struct termios oldTio;
	pid_t mainPgrp;
} ShellState;

ShellState *new_ShellState();
void free_ShellState(ShellState *state);

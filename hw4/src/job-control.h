#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "job.h"
#include "state.h"

void addJob(ShellState *state, Job *job);
void removeJob(ShellState *state, Job *job);
Job* findJobByPid(ShellState *state, pid_t pid);
Job* findJobByJid(ShellState *state, int jid);

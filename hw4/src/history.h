#pragma once

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "state.h"
#include "token.h"
#include "history-entry.h"

#define HISTORY_FILE "~/.320sh_history"

void addHistoryEntry(ShellState *state, TokenList *tokens);
void printHistory(ShellState *state);
int writeHistory(ShellState *state);
int readHistory(ShellState *state);
void clearHistory(ShellState *state);

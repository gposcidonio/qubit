#include "history.h"

void addHistoryEntry(ShellState *state, TokenList *tokens) {
	if (tokens == NULL) return;

	TokenList *tokenCursor;
	size_t numChars = 256;
	char *text = malloc(sizeof(char) * numChars);
	memset(text, 0, numChars);

	for (
		tokenCursor = tokens;
		tokenCursor != NULL;
		tokenCursor = tokenCursor->next
	) {
		// if the next token is too big for the entry, embiggen the entry
		if (strlen(text) + strlen(tokenCursor->data) + 3 > numChars) {
			char *temp = malloc(numChars * 2);
			numChars *= 2;
			memcpy(temp, text, strlen(text) + 1);
			free(text);
			text = temp;
		}

		strcat(text, tokenCursor->data);
		if (tokenCursor->type == PIPE) {
			strcat(text, "|");
		} else if (tokenCursor->type == FILE_IN) {
			strcat(text, "<");
		} else if (tokenCursor->type == FILE_OUT) {
			strcat(text, ">");
		}
		if (tokenCursor->next != NULL) strcat(text, " ");
	}

	HistoryEntry *entry = new_HistoryEntry(text);

	if (state->history == NULL) {
		entry->position = 0;
		state->history = entry;
		state->historyTail = entry;
	} else {
		entry->position = state->historyTail->position + 1;
		state->historyTail->next = entry;
		entry->prev = state->historyTail;
		state->historyTail = entry;
	}
}

void printHistory(ShellState *state) {
	if (state->history == NULL) {
		printf("History is empty.\n");
	} else {
		HistoryEntry* cursor;
		for (
			cursor = state->history;
			cursor != NULL;
			cursor = cursor->next
		) {
			printf("  %3d %s\n", cursor->position + 1, cursor->text);
		}
	}
}

int writeHistory(ShellState *state) {
	char *homeDir = getenv("HOME");
	size_t pathLen = strlen(homeDir) + strlen("/.320sh_history");
	char *filePath = malloc(sizeof(char) * pathLen + 1);
	sprintf(filePath, "%s/.320sh_history", homeDir);

	FILE *fp;
	if ((fp = fopen(filePath, "w+")) == NULL) {
		printf("write in history: errno: %d\n", errno);
		return 1;
	}

	HistoryEntry *cursor;
	for (cursor = state->history; cursor != NULL; cursor = cursor->next) {
		fprintf(fp, "%s\n", cursor->text);
	}

	fclose(fp);
	return 0;
}

int readHistory(ShellState *state) {
	char *homeDir = getenv("HOME");
	size_t pathLen = strlen(homeDir) + strlen("/.320sh_history");
	char *filePath = malloc(sizeof(char) * pathLen + 1);
	sprintf(filePath, "%s/.320sh_history", homeDir);

	FILE *fp;
	if ((fp = fopen(filePath, "r")) == NULL) {
		if (errno != ENOENT) printf("read in history: errno: %d\n", errno);
		return 1;
	}

	// clear history if it exists
	if (state->history != NULL) {
		free_HistoryEntry(state->history);
		state->history = NULL;
		state->historyTail = NULL;
	}

	int index = 0;
	char *line = NULL;
	size_t len = 0;
	for (index = 0; (len = getline(&line, &len, fp)) != -1; index++) {
		HistoryEntry *entry = new_HistoryEntry(line);
		entry->position = index;

		entry->text = malloc(sizeof(char) * (len + 1));
		strcpy(entry->text, line);
		entry->text[len - 1] = '\0';

		if (index == 0) {
			state->history = entry;
			state->historyTail = entry;
		} else {
			state->historyTail->next = entry;
			entry->prev = state->historyTail;
			state->historyTail = entry;
		}
	}

	fclose(fp);
	return 0;
}

void clearHistory(ShellState *state) {
	unlink(HISTORY_FILE);
	free_HistoryEntry(state->history);
	state->history = NULL;
	state->historyTail = NULL;
}

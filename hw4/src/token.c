#include "token.h"

TokenList *new_TokenList(void) { return new_TokenListw(128); }
TokenList *new_TokenListw(size_t width) {
	TokenList *list = malloc(sizeof(TokenList));
	list->data = malloc(sizeof(char) * width);
	list->width = width;
	list->type = STANDARD;
	list->next = NULL;

	return list;
}

TokenList *new_TokenListd(char *data) {
	TokenList *token = new_TokenListw(strlen(data) + 1);
	strcpy(token->data, data);
	return token;
}

void free_TokenList(TokenList *tokenList) {
	assert(tokenList != NULL);
	free(tokenList->data);

	TokenList *next = tokenList->next;
	free(tokenList);
	if (next != NULL) free_TokenList(next);
}

#pragma once

#include <stdlib.h>

typedef struct HistoryEntry {
	char *text;
	int position;
	struct HistoryEntry* next;
	struct HistoryEntry* prev;
} HistoryEntry;

HistoryEntry *new_HistoryEntry(char *text);
void free_HistoryEntry(HistoryEntry *entry);

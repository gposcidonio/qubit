#include "trie.h"

TrieNode *new_TrieNode() {
	TrieNode *node = malloc(sizeof(TrieNode));
	node->stringExists = 0;
	node->numChildren = 0;
	for (int i = 0; i < TRIE_NCHILDREN; i++) node->children[i] = NULL;
	return node;
}

void free_TrieNode(TrieNode *node) {
	for (int i = 0; i < TRIE_NCHILDREN; i++) {
		if (node->children[i] != NULL) {
			free_TrieNode(node->children[i]);
		}
	}
	free(node);
}

int trieHasString(TrieNode *trie, char *str) {
	while (*str && trie != NULL) {
		trie = trie->children[(int)*str];
		str++;
	}

	if (trie == NULL) return 0;
	else /*str == '\0'*/ return trie->stringExists;
}

void addStringToTrie(TrieNode *trie, char *str) {
	while (*str) {
		if (trie->children[(int)*str] == NULL) {
			trie->children[(int)*str] = new_TrieNode();
			trie->numChildren++;
		}
		trie = trie->children[(int)*str];
		str++;
	}

	trie->stringExists = 1;
}

StringList *getStringsInTrieHelper(
	TrieNode *trie,
	char *strSoFar,
	size_t strSoFarIndex,
	size_t strSoFarSize
) {
	StringList *list = NULL;
	StringList *endOfList = NULL;
	int doubled = 0;

	if (strSoFarIndex >= strSoFarSize) {
		doubled = 1;
		char *newStr = malloc(sizeof(char) * strSoFarSize * 2);
		if (strSoFar != NULL) strncpy(newStr, strSoFar, strSoFarSize);
		strSoFarSize <<= 1;
		strSoFar = newStr;
	}

	if (trie->stringExists) {
		list = new_StringList(strSoFar, strSoFarIndex);
		endOfList = list;
	}

	for (int i = 0; i < TRIE_NCHILDREN; i++) {
		if (trie->children[i] != NULL) {
			strSoFar[strSoFarIndex] = i;
			StringList *newStringList = getStringsInTrieHelper(
				trie->children[i],
				strSoFar,
				strSoFarIndex + 1,
				strSoFarSize
			);

			if (newStringList != NULL) {
				StringList *tmp = newStringList;
				while (tmp->next != NULL) tmp = tmp->next;

				if (list == NULL) {
					list = newStringList;
					endOfList = tmp;
				} else {
					endOfList->next = newStringList;
					endOfList = tmp;
				}
			}
		}
	}

	if (doubled) free(strSoFar);
	return list;
}

StringList *getStringsInTrie(TrieNode *trie) {
	char *strSoFar = malloc(sizeof(char) * 1024);
	StringList *strings = getStringsInTrieHelper(trie, strSoFar, 0, 1024);
	free(strSoFar);
	return strings;
}

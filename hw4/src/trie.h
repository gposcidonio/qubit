#pragma once

#include <stdlib.h>
#include <limits.h>

#include "stringlist.h"

#define TRIE_NCHILDREN UCHAR_MAX

typedef struct TrieNode {
	int stringExists;
	unsigned char numChildren;
	struct TrieNode *children[TRIE_NCHILDREN];
} TrieNode;

TrieNode *new_TrieNode();
void free_TrieNode(TrieNode *trie);
int trieHasString(TrieNode *trie, char *str);
void addStringToTrie(TrieNode *trie, char *str);
StringList *getStringsInTrie(TrieNode *trie);

#pragma once

#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>

#include "trie.h"
#include "state.h"
#include "prompt.h"
#include "job-control.h"
#include "job.h"
#include "global-state.h"

char *readline(ShellState *state);
size_t tabComplete(
	ShellState *state,
	char **buf,
	size_t bufIndex,
	size_t bufSize
);

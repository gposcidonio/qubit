#include "stringlist.h"

StringList *new_StringList(char *str, size_t len) {
	StringList *strList = malloc(sizeof(StringList));
	strList->next = NULL;

	strList->str = malloc(sizeof(char) * (len + 1));
	strncpy(strList->str, str, len);
	strList->str[len] = '\0';

	return strList;
}

void free_StringList(StringList *list) {
	StringList *next = list->next;
	free(list->str);
	free(list);
	if (next != NULL) free_StringList(next);
}

#include "job.h"

Job *new_Job(int pid, char* name, ExecutionMode mode){
	Job *job = malloc(sizeof(Job));
	job->pid = pid;
	job->name = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(job->name, name);
	job->jid = 0;
	job->next = NULL;
	job->prev = NULL;
	job->mode = mode;
	job->status = RUNNING;
	return job;
}

void free_Job(Job *job){
	if(job->prev != NULL) job->prev->next = job->next;
	if(job->next != NULL) job->next->prev = job->prev;
	free(job);
}

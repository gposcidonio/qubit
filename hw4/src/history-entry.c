#include "history-entry.h"

HistoryEntry *new_HistoryEntry(char *text) {
	HistoryEntry *entry = malloc(sizeof(HistoryEntry));
	entry->text = text;
	entry->next = NULL;
	entry->prev = NULL;
	return entry;
}

void free_HistoryEntry(HistoryEntry *entry) {
	free(entry->text);
	HistoryEntry *next = entry->next;
	free(entry);
	if (next != NULL) free_HistoryEntry(next);
}

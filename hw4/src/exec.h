#pragma once

#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/resource.h>

#include "token.h"
#include "state.h"
#include "quit.h"
#include "history.h"
#include "command.h"
#include "job.h"
#include "job-control.h"

void execute(ShellState *state, TokenList *tokens);
TokenList *expandGlobs(TokenList *tokens);
pid_t runBuiltin(ShellState *state, CommandList *command);
CommandList *parseCommands(ShellState *state, TokenList *tokens);
void executeCommands(ShellState *state, TokenList *tokens);
pid_t executeCommand(ShellState *state, CommandList *tokens);

void printCommandList(CommandList *list);

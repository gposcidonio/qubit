#pragma once

#include <stdlib.h>

#include "token.h"
#include "exec-mode.h"

typedef struct CommandList {
	TokenList *tokens;
	struct CommandList *next;
	ExecutionMode mode;
	int fds[3];
} CommandList;

CommandList *new_CommandList();
void free_CommandList(CommandList *list);

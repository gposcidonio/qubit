#include "exec.h"

void execute(ShellState *state, TokenList *tokens) {
	assert(tokens != NULL);

	// if there's nothing to do, do nothing
	if (strlen(tokens->data) == 0) {
		return;
	}

	expandGlobs(tokens);

	// store tokens in history
	addHistoryEntry(state, tokens);

	int status;
	int pid;
	while((pid = waitpid(-1, &status, WNOHANG)) != 0){
		if(pid == -1 && errno == ECHILD) break;
		if (!WIFEXITED(status)) continue;
		Job *job = findJobByPid(state, pid);
		assert(job != NULL);
		removeJob(state, job);
		if(job->mode == BACKGROUND){
			free_Job(job);
			continue;
		}
	}

	// parse tokens into commands
	CommandList *list = parseCommands(state, tokens);
	if (list == NULL) return;

	struct termios curTio;
	if (state->mode == SHELL) {
		// turn echoing and line buffering on
		struct termios newTio;
		tcgetattr(0, &newTio);
		curTio = newTio;
		newTio.c_lflag |= ICANON | ECHO;
		tcsetattr(STDIN_FILENO, TCSANOW, &newTio);
	}

	struct rusage usage;
	struct timeval startTotal, startUser, startSys;
	struct timeval endTotal, endUser, endSys;
	if(state->timeCounting){
		getrusage(RUSAGE_CHILDREN, &usage);
		startUser = usage.ru_utime;
		startSys = usage.ru_stime;
		gettimeofday(&startTotal, NULL);
	}

	pid_t pgrp = 0;
	CommandList *command;
	for (command = list; command != NULL; command = command->next) {
		pid_t pid;
		pid = runBuiltin(state, command);
		if (pid < 0) {
			pid = executeCommand(state, command);
		}

		// if we failed to find a command, halt
		if (pid == 0) {
			break;
		}

		char *name = command->tokens->data;
		Job *job = new_Job(pid, name, command->mode);
		addJob(state, job);

		// handle pgrps
		if (pgrp == 0) {
			pgrp = pid;
			if (job->mode == FOREGROUND) {
				//tcsetpgrp(STDIN_FILENO, pgrp);
			}
		}
		job->pgrp = pgrp;
		setpgid(pid, pgrp);
	}

	Job *job;
	int numCmds = 0;
	for (job = state->jobList; job != NULL; job = job->next) {
		if (job->mode == FOREGROUND) {
			numCmds++;
		}
	}
	// wait for commands to terminate
	while (numCmds > 0) {
		int exitInfo;
		pid_t pid = waitpid(-1, &exitInfo, WNOHANG);
		if (pid != 0) {
			if (!WIFEXITED(exitInfo)) continue;
			job = findJobByPid(state, pid);
			assert(job != NULL);

			// TODO print out job stuff
			removeJob(state, job);
			if(job->mode == BACKGROUND){
				free_Job(job);
				continue;
			}

			char *exitStatusString = malloc(sizeof(char) * 1024);
			sprintf(exitStatusString, "%i", WEXITSTATUS(exitInfo));
			setenv("?", exitStatusString, 1);
			free(exitStatusString);

			numCmds--;
			if (state->debug) {
				// TODO print out full command text, not just first token
				fprintf(
					stderr,
					"ENDED: %s (ret=%d)\n",
					job->name,
					WEXITSTATUS(exitInfo)
				);
			}
			free_Job(job);
		}

		// check if we ^C'd our foreground processes
		numCmds = 0;
		for (job = state->jobList; job != NULL; job = job->next) {
			if (job->mode == FOREGROUND) {
				numCmds++;
			}
		}
	}
	if(state->timeCounting){
		getrusage(RUSAGE_CHILDREN, &usage);
		endUser = usage.ru_utime;
		endSys = usage.ru_stime;
		gettimeofday(&endTotal, NULL);

		time_t elapsedTotalTime = endTotal.tv_sec - startTotal.tv_sec;
		elapsedTotalTime *= 1000000L;
		elapsedTotalTime += endTotal.tv_usec;
		elapsedTotalTime -= startTotal.tv_usec;

		time_t elapsedSysTime = endSys.tv_sec - startSys.tv_sec;
		elapsedSysTime *= 1000000L;
		elapsedSysTime += endSys.tv_usec;
		elapsedSysTime -= startSys.tv_usec;

		time_t elapsedUsrTime = endUser.tv_sec - startUser.tv_sec;
		elapsedUsrTime *= 1000000L;
		elapsedUsrTime -= startUser.tv_usec;
		elapsedUsrTime += endUser.tv_usec;

		double total, usr, sys;

		total = (double)(elapsedTotalTime) / 1000000.0;
		sys = (double)(elapsedSysTime) / 1000000.0;
		usr = (double)(elapsedUsrTime) / 1000000.0;

		printf("TIME: real=%.2fs user=%.2fs sys=%.2fs\n", total, usr, sys);
	}

	//pid_t ret = tcsetpgrp(STDIN_FILENO, state->mainPgrp);
	//if (ret == -1) perror(NULL);

	if (state->mode == SHELL) {
		// reset stdin settings
		tcsetattr(STDIN_FILENO, TCSANOW, &curTio);
	}

	free_CommandList(list);
}

pid_t runBuiltin(ShellState *state, CommandList *command) {
	TokenList *tokens = command->tokens;
	assert(tokens != NULL);

	int numTokens = 1;
	TokenList *tmp = tokens;
	while ((tmp = tmp->next) != NULL) numTokens++;

	// builtins
	if (strcmp(tokens->data, "exit") == 0) {
		if (numTokens > 1) {
			printf("exit: too many arguments\n");
			return 0;
		}

		free_CommandList(command);
		quit(state);
	} else if (strcmp(tokens->data, "cd") == 0) {
		char *cwd = getcwd(NULL, 0);
		int cded = 0;

		if (numTokens == 1) {
			chdir(getenv("HOME"));
			cded = 1;
		} else if (numTokens == 2) {
			char *targetDir = tokens->next->data;

			if (strcmp(targetDir, "-") == 0 && state->prevDir != NULL) {
				// if "cd -": cd to prevDir
				chdir(state->prevDir);
				cded = 1;
			} else {
				// else cd to the named dir
				int ret = chdir(targetDir);
				if (ret == -1) {
					switch (errno) {
						case ENOENT:
							printf("cd: no such file or directory: ");
							printf("%s\n", targetDir);
							break;
						case ENOTDIR:
							printf("cd: not a directory: ");
							printf("%s\n", targetDir);
							break;
						default:
							printf("cd: there was an error\n");
					}
				} else {
					cded = 1;
				}
			}
		} else if (numTokens > 2) {
			printf("cd: too many arguments\n");
			return 0;
		}

		if (cded) {
			if (state->prevDir == NULL) free(state->prevDir);
			state->prevDir = cwd;
		}
	} else if (strcmp(tokens->data, "pwd") == 0) {
		char *cwd = getcwd(NULL, 0);
		printf("%s\n", cwd);
		free(cwd);
	} else if (strcmp(tokens->data, "history") == 0) {
		pid_t pid = fork();
		int i;
		if (pid != 0) {
			// we are the parent
			// clean up redirects
			for (i = 0; i < 3; i++) {
				if (command->fds[i] > 2) {
					close(command->fds[i]);
				}
			}

			return pid;
		}

		// handle redirects
		for (i = 0; i < 3; i++) {
			if (command->fds[i] > 2) {
				close(i);
				dup(command->fds[i]);
				close(command->fds[i]);
			}
		}

		printHistory(state);
		exit(0);
	} else if (strcmp(tokens->data, "clear-history") == 0) {
		if (numTokens > 1) {
			printf("clear-history: too many arguments\n");
			return 0;
		}
		clearHistory(state);
	} else if (strcmp(tokens->data, "echo") == 0) {
		pid_t pid = fork();
		int i;
		if (pid != 0) {
			// we are the parent
			// clean up redirects
			for (i = 0; i < 3; i++) {
				if (command->fds[i] > 2) {
					close(command->fds[i]);
				}
			}

			return pid;
		}

		// handle redirects
		for (i = 0; i < 3; i++) {
			if (command->fds[i] > 2) {
				close(i);
				dup(command->fds[i]);
				close(command->fds[i]);
			}
		}

		tmp = tokens;
		while ((tmp = tmp->next) != NULL) {
			printf("%s ",tmp->data);
		}
		printf("\n");
		exit(0);
	} else if (strcmp(tokens->data, "set") == 0) {
		if (numTokens == 1) {
			// default functionality if no args provided
			// just print out all the variables
			// (this is what bash does)
			char **tempStr = state->envVars;
			while (*tempStr != NULL) {
				printf("%s\n", *tempStr);
				tempStr++;
			}
		} else {
			// time to assign some variables
			tmp = tokens;
			while ((tmp = tmp->next) != NULL) {
				// grab the tokenLen before we do strtok.
				// we're gonna need it.
				int tokenLen = strlen(tmp->data);
				char *var = strtok(tmp->data, "=");
				if (strlen(var) != tokenLen) {
					// if the length of the variable name
					// is not the same as the tokenLen
					// (meaning that there was an '=' in
					// the token)
					if (strlen(var) + 1 == tokenLen) {
						// case: variable= value
						setenv(var, tmp->next->data, 1);
						// set the cursor past all the variables used.
						tmp = tmp->next;
					} else {
						// case: variable=value
						setenv(var, tmp->data + strlen(var) + 1, 1);
					}
				} else {
					// if we are here, then the equals sign must be at the
					// beginning of the next token
					TokenList* tmp2 = tmp->next;
					if (tmp2 != NULL && *(tmp2->data) == '=') {
						if (strlen(tmp2->data) == 1) {
							// case: variable = value
							setenv(var, tmp2->next->data, 1);
							// set the cursor past all the variables used.
							tmp = tmp2->next;
						} else {
							// case: variable =value
							setenv(var, (tmp2->data + 1), 1);
							// set the cursor past all the variables used.
							tmp = tmp2;
						}
					} else {
						printf("set: invalid arguments\n");
						return 0;
					}
				}
			}
		}
	} else if (strcmp(tokens->data, "jobs") == 0){
		Job *currentJob = state->jobList;
		while(currentJob != NULL){
			int exitInfo;
			pid_t result = waitpid(currentJob->pid, &exitInfo, WNOHANG);
			if(result > 0){
				if(WIFSTOPPED(exitInfo) == 0){
					currentJob->status = STOPPED;
				} else if (WIFCONTINUED(exitInfo) == 0){
					currentJob->status = RUNNING;
				}
			}
			char* status = currentJob->status == RUNNING ? "Running" : "Stopped";
			printf("[%d] (%d)\t%s\t\t%s\n", currentJob->jid, currentJob->pid, status, currentJob->name);
			currentJob = currentJob->next;
		}
	} else if (strcmp(tokens->data, "kill") == 0){
		if(numTokens == 1) {
			printf("kill: not enough arguments\n");
			return 0;
		}

		tmp = tokens->next;
		while(tmp != NULL){
			Job *job = NULL;
			if(*tmp->data == '%'){
				char *jidStr = tmp->data + 1;
				int jid = atoi(jidStr);
				job = findJobByJid(state, jid);
			} else {
				pid_t pid = atoi(tmp->data);
				job = findJobByPid(state, pid);
			}

			if(job == NULL){
				printf("kill: no such job %s\n", tmp->data);
				return 0;
			}

			if (kill(job->pid, SIGKILL) == -1) {
				perror("kill");
			} else {
				removeJob(state, job);
			}
			tmp = tmp->next;
		}
	} else if (strcmp(tokens->data, "fg") == 0){
		if(numTokens == 1){
			printf("fg: not enough arguments\n");
			return 0;
		} else if (numTokens > 2) {
			printf("fg: too many arguments\n");
			return 0;
		}

		tmp = tokens->next;
		Job *job = NULL;
		if (*tmp->data == '%'){
			// find by jid
			char *jidStr = tmp->data + 1;
			int jid = atoi(jidStr);
			job = findJobByJid(state, jid);
		} else {
			job = findJobByPid(state, atoi(tmp->data));
		}

		if(job == NULL){
			printf("fg: no such job %s\n", tmp->data);
			return 0;
		}

		if (job->mode == FOREGROUND && job->status == RUNNING) {
			printf("fg: job is already running\n");
			return 0;
		}

		job->status = RUNNING;
		job->mode = FOREGROUND;
		//tcsetpgrp(STDIN_FILENO, job->pgrp);
		kill(job->pid, SIGCONT);
		return 0;
	} else if (strcmp(tokens->data, "bg") == 0){
		if(numTokens == 1){
			printf("bg: not enough arguments\n");
			return 0;
		}
		tmp = tokens->next;
		while(tmp != NULL){
			Job *job = NULL;
			if(*tmp->data == '%'){
				char *jidStr = tmp->data + 1;
				int jid = atoi(jidStr);
				job = findJobByJid(state, jid);
			} else {
				job = findJobByPid(state, atoi(tmp->data));
			}

			if(job == NULL){
				printf("bg: no such job %s\n", tmp->data);
				return 0;
			}

			if (job->mode == BACKGROUND && job->status == RUNNING) {
				printf("bg: job is already running\n");
				return 0;
			}

			job->status = RUNNING;
			job->mode = BACKGROUND;
			kill(job->pid, SIGCONT);
			tmp = tmp->next;
		}

	} else if (strcmp(tokens->data, "stop") == 0){
		if(numTokens == 1){
			printf("stop: not enough arguments\n");
			return 0;
		}
		tmp = tokens->next;
		while(tmp != NULL){
			Job *job;
			if (*tmp->data == '%'){
				char *jidStr = tmp->data + 1;
				int jid = atoi(jidStr);
				job = findJobByJid(state, jid);
			} else {
				job = findJobByPid(state, atoi(tmp->data));
			}

			if (job == NULL){
				printf("stop: no such job %s\n", tmp->data);
				return 0;
			}

			job->status = STOPPED;
			job->mode = BACKGROUND;
			kill(job->pid, SIGSTOP);
			tmp = tmp->next;
		}
	} else {
		return -1;
	}

	return 0;
}

CommandList *parseCommands(ShellState *state, TokenList *tokens) {
	assert(tokens != NULL);

	CommandList *list = new_CommandList(tokens);
	CommandList *curCommand = list;
	TokenList *prev = NULL;

	// lines mustn't start with a redirect
	if (tokens->type != STANDARD) {
		printf("error: started with a redirect\n");
		free_CommandList(list);
		return NULL;
	}

	// get the commands {
	while (tokens != NULL) {
		if(
				tokens->type != STANDARD &&
				((tokens->next != NULL && tokens->next->type != STANDARD) ||
				prev == NULL)
		){
			char c = '\0';
			if (prev->type == PIPE) c = '|';
			else if (prev->type == FILE_IN) c = '<';
			else if (prev->type == FILE_OUT) c = '>';
			else if (prev->type == AMPERSAND) c = '&';
			printf("error: syntax error near %c\n", c);
		}

		if (tokens->type == PIPE) {
			if (tokens->next == NULL) {
				// pipe to what? parse error
				printf("error: dangling pipe\n");
				free_CommandList(list);
				return NULL;
			}

			// set up pipe
			int pipeFds[2];
			pipe(pipeFds);
			// check for file descriptor specification
			int newfd = atoi(prev->data);
			if (*tokens->data == '\0' || newfd > 2) newfd = 1;
			curCommand->fds[newfd] = pipeFds[1];

			// start a new command
			prev->next = NULL;
			curCommand->next = new_CommandList(tokens->next);
			curCommand = curCommand->next;
			curCommand->fds[0] = pipeFds[0];
		} else if (tokens->type == FILE_OUT || tokens->type == FILE_IN) {
			// FILE_OUT or FILE_IN should be ignored from the token list
			if (tokens->next != NULL) prev->next = tokens->next->next;

			// TODO handle multiple io targets
			// set the relevant file descriptors
			int oldfd = atoi(tokens->data);
			int newfd;
			if (tokens->type == FILE_IN) {
				if (*tokens->data == '\0' || oldfd > 2) oldfd = 0;
				newfd = open(tokens->next->data, O_RDONLY, 0666);
			} else if (tokens->type == FILE_OUT) {
				if (*tokens->data == '\0' || oldfd > 2) oldfd = 1;
				newfd = open(
						tokens->next->data,
						O_WRONLY | O_CREAT | O_TRUNC,
						0666
				);
			} else {
				assert(0); // we can never get here
			}
			curCommand->fds[oldfd] = newfd;
		} else if (tokens->type == AMPERSAND){
			list->mode = BACKGROUND;
			prev->next = NULL;
			if(tokens->next != NULL)
				curCommand->next = new_CommandList(tokens->next);
			curCommand = curCommand->next;
		}
		// step forward
		TokenList *next = tokens->next;
		if (tokens->type == STANDARD) prev = tokens;
		else if (tokens->type == FILE_IN || tokens->type == FILE_OUT) {
			// skip the filename token
			next = next->next;
			tokens->next->next = NULL;
			free_TokenList(tokens);
		} else if (tokens->type == PIPE) {
			tokens->next = NULL;
			free_TokenList(tokens);
		}
		tokens = next;
	}

	return list;
}

TokenList *expandGlobs(TokenList *tokens) {
	TokenList *prev = NULL;
	TokenList *curToken = tokens;
	while (curToken != NULL) {
		if (curToken->type == STANDARD && curToken->data[0] == '*') {
			// replace this token with all the matching filenames
			char *ext = curToken->data + 1;
			size_t extLen = strlen(ext);
			DIR *dir = opendir(".");
			struct dirent *entry;

			TokenList *newTokens = NULL;
			TokenList *lastNewToken = NULL;
			while ((entry = readdir(dir)) != NULL) {
				size_t nameLen = strlen(entry->d_name);
				char *entryExt = entry->d_name + nameLen - extLen;
				if (strcmp(ext, entryExt) == 0) {
					// match found, add to the list
					TokenList *token = new_TokenListd(entry->d_name);
					token->next = newTokens;
					newTokens = token;
					if (lastNewToken == NULL) lastNewToken = token;
				}
			}

			if (prev == NULL) tokens = newTokens;
			else prev->next = newTokens;
			lastNewToken->next = curToken->next;
			curToken = lastNewToken;
		}

		prev = curToken;
		curToken = curToken->next;
	}

	return tokens;
}

pid_t executeCommand(ShellState *state, CommandList *command) {
	TokenList *tokens = command->tokens;

	int numTokens = 1;
	TokenList *tmp = tokens;
	while ((tmp = tmp->next) != NULL) numTokens++;

	// get clone of PATH
	char *actualPath = getenv("PATH");
	char *pathClone = malloc(sizeof(char) * (strlen(actualPath) + 1));
	strcpy(pathClone, actualPath);
	char *originalPathClone = pathClone;

	// search PATH for file
	char *searchDir;
	int found = 0;
	while ((searchDir = strsep(&pathClone, ":")) != NULL) {
		// scan searchDir
		DIR *dir = opendir(searchDir);
		struct dirent *entry;

		// check each file
		while ((entry = readdir(dir)) != NULL && !found) {
			// ignore directories
			if (entry->d_type == DT_DIR) continue;

			if (strcmp(tokens->data, entry->d_name) == 0) {
				// we found it
				found = 1;
			}
		}

		closedir(dir);
		if (found) break;
	}

	char *filePath;
	if (found) {
		// found it in PATH
		size_t dirLen = strlen(searchDir);
		size_t fileLen = strlen(tokens->data);
		filePath = malloc(sizeof(char) * (dirLen + fileLen + 2));
		strcpy(filePath, searchDir);
		filePath[dirLen] = '/';
		filePath[dirLen + 1] = '\0';
		strcat(filePath, tokens->data);
	} else {
		// not found in PATH, check current dir
		struct stat buf;
		found = !lstat(tokens->data, &buf);
		filePath = malloc(sizeof(char) * (strlen(tokens->data) + 1));
		strcpy(filePath, tokens->data);
	}

	pid_t childId = 0;
	if (found) {
		// get the argList
		char **argList = malloc(sizeof(char *) * (numTokens + 1));
		tmp = tokens;
		int i;
		for (i = 0; i < numTokens; i++) {
			argList[i] = tmp->data;
			tmp = tmp->next;
		}
		argList[numTokens] = NULL;

		if (state->debug) {
			fprintf(stderr, "RUNNING: %s\n", tokens->data);
		}

		// exec the file
		childId = fork();
		if (childId == 0) {
			// we are the child
			// change fds as requested
			for (i = 0; i < 3; i++) {
				if (command->fds[i] > 2) {
					close(i);
					dup(command->fds[i]);
					close(command->fds[i]);
				}
			}
			execv(filePath, argList);
		}

		// we are not the child
		// close any open files for this command
		for (i = 0; i < 3; i++) {
			if (command->fds[i] > 2) {
				close(command->fds[i]);
			}
		}

		free(argList);
		free(filePath);
	} else {
		// if command not found, say fnf
		printf("320sh: command not found: %s\n", tokens->data);
	}

	free(originalPathClone);
	return childId;
}

void printCommandList(CommandList *list) {
	while (list != NULL) {
		printf(
				"command: %d %d %d\n",
				list->fds[0],
				list->fds[1],
				list->fds[2]
		);
		TokenList *tokens = list->tokens;
		while (tokens != NULL) {
			if (tokens->type == STANDARD) printf("STANDARD ");
			else if (tokens->type == PIPE) printf("PIPE ");
			else if (tokens->type == FILE_OUT) printf("FILE_OUT ");
			else if (tokens->type == FILE_IN) printf("FILE_IN ");
			printf("%s\n", tokens->data);
			tokens = tokens->next;
		}
		printf("\n");
		list = list->next;
	}
}

#include "main.h"

void signalHandler(int signal) {
	// Control-C and Control-Z
	Job *job;
	for (job = global_state->jobList; job != NULL; job = job->next) {
		if (job->mode == FOREGROUND) {

			int result = kill(job->pid, signal);
			if (signal == SIGINT) result = kill(job->pid, SIGINT);
			else if (signal == SIGTSTP) result = kill(job->pid, SIGTSTP);
			else printf("signal %i not handled\n", signal);

			if (result == -1) {
				perror("320sh");
				job = job->next;
				continue;
			}

			if (signal == SIGINT) {
				removeJob(global_state, job);
				if (global_state->debug) {
					fprintf(
						stderr,
						"ENDED: %s (ret=%d)\n",
						job->name,
						130
					);
				}
			} else job->status = STOPPED;
		}
	}
}

int main(int argc, char **argv, char **envp) {
	int running = 1;
	TokenList *tokens;
	struct termios newTio;
	ShellState *state = new_ShellState();
	state->envVars = envp;
	state->mainPgrp = tcgetpgrp(STDIN_FILENO);
	global_state = state;

	readHistory(state);

	// check for flags
	int opt;
	while ((opt = getopt(argc, argv, "dt")) != -1) {
		switch (opt) {
			case 'd':
				state->debug = 1;
				break;
			case 't':
				state->timeCounting = 1;
				break;
			default:
				return EXIT_FAILURE;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc == 1) {
		state->inputFile = fopen(argv[0], "r");
		state->mode = SCRIPT;
	} else if (argc > 1) {
		printf("error: too many inputs\n");
	} else {
		// shell mode
		// disable echoing and line buffering
		tcgetattr(0, &state->oldTio);
		newTio = state->oldTio;
		newTio.c_lflag &= ~ICANON & ~ECHO;
		tcsetattr(STDIN_FILENO, TCSANOW, &newTio);
	}

	signal(SIGINT, signalHandler);
	signal(SIGTSTP, signalHandler);

	while (running) {
		if (state->mode == SHELL) printPrompt();

		char *line = readline(state);
		tokens = parse(state, line);
		if (tokens == NULL) {
			continue;
		}

		execute(state, tokens);
	}

	quit(state);
	return EXIT_SUCCESS;
}

#include "job-control.h"

void addJob(ShellState *state, Job *newJob){
	int i;
	Job *currentJob = state->jobList;
	Job *prevJob = NULL;

	for(i = 1; currentJob != NULL && i == currentJob->jid; i++){
		prevJob = currentJob;
		currentJob = currentJob->next;
	}

	newJob->jid = i;
	newJob->prev = prevJob;
	newJob->next = currentJob;
	if(prevJob != NULL) prevJob->next = newJob;
	if(currentJob != NULL) currentJob->prev = newJob;
	if(currentJob == state->jobList) state->jobList = newJob;
}

void removeJob(ShellState *state, Job *job) {
	assert(job != NULL);
	if(job == state->jobList){
		state->jobList = job->next;
	}
	if(job->next != NULL) job->next->prev = job->prev;
	if(job->prev != NULL) job->prev->next = job->next;
}

Job* findJobByPid(ShellState *state, pid_t pid){
	Job* currentJob = state->jobList;
	while(currentJob != NULL){
		if(currentJob->pid == pid) return currentJob;
		currentJob = currentJob->next;
	}
	return NULL;
}

Job* findJobByJid(ShellState *state, int jid){
	Job* currentJob = state->jobList;
	while(currentJob != NULL){
		if(currentJob->jid == jid) return currentJob;
		currentJob = currentJob->next;
	}
	return NULL;
}

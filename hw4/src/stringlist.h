#pragma once

#include <string.h>
#include <stdlib.h>

typedef struct StringList {
	char *str;
	struct StringList *next;
} StringList;

StringList *new_StringList(char *str, size_t len);
void free_StringList(StringList *list);

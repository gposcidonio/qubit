#include "state.h"

ShellState *new_ShellState() {
	ShellState *state = malloc(sizeof(ShellState));
	state->prevDir = NULL;
	state->debug = 0;
	state->mode = SHELL;
	state->inputFile = stdin;
	state->history = NULL;
	state->historyTail = NULL;
	state->mainPgrp = 0;
	state->timeCounting = 0;

	return state;
}

void free_ShellState(ShellState *state) {
	if (state->prevDir != NULL) free(state->prevDir);
	if (state->history != NULL) free_HistoryEntry(state->history);
	free(state);
}

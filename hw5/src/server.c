#include "server.h"

int main(int argc, char **argv) {
	ServerState *state = new_ServerState();
	char *name = *argv;

	// handle flags
	int opt;
	while ((opt = getopt(argc, argv, "eh")) != -1) {
		switch (opt) {
			case 'e':
				state->echoToStdout = 1;
				break;

			case 'h':
				usage(name);
				quit(state, EXIT_FAILURE);
				break;

			default:
				printf("error: bad flag\n");
				return EXIT_FAILURE;
		}
	}
	argc -= optind;
	argv += optind;

	// check argcount
	if (argc != 2) {
		usage(name);
		return EXIT_FAILURE;
	}

	// check if port is valid
	state->port = atoi(argv[0]);
	state->motd = argv[1];
	if (state->port > MAX_PORT) {
		printf("error: port must be between 0 and %i\n", MAX_PORT);
		return EXIT_FAILURE;
	}

	printf("starting server on port %i\n", state->port);

	// open a socket for listening
	if (openListen(state) < 0) {
		printf("server: error starting server\n");
		quit(state, EXIT_FAILURE);
	}

	// listen for connections
	struct sockaddr clientAddr;
	socklen_t clientAddrLen = sizeof(clientAddr);
	while (1) {
		int clientFd = accept(
			state->listenFd,
			&clientAddr,
			&clientAddrLen
		);

		if (clientFd == -1) {
			perror("server");
			quit(state, EXIT_FAILURE);
		}

		// start the login thread
		LoginStruct *loginStruct = malloc(sizeof(LoginStruct));
		loginStruct->state = state;
		loginStruct->clientFd = clientFd;
		pthread_t tid;
		pthread_create(&tid, NULL, login, loginStruct);
	}

	printf("server ending\n");
	quit(state, EXIT_SUCCESS);

	// should never get here
	return EXIT_FAILURE;
}

int openListen(ServerState *state) {
	// open socket
	state->listenFd = socket(AF_INET, SOCK_STREAM, 6);
	if (state->listenFd < 0) {
		perror("server");
		return -1;
	}

	// configure socket to listen for connections to any local ip
	struct sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(state->port);
	if (
		bind(
			state->listenFd,
			(struct sockaddr *)&serverAddr,
			sizeof(serverAddr)
		) < 0
	) {
		perror("server");
		return -1;
	}

	// start the socket listening for connections
	if (listen(state->listenFd, 1024) < 0) {
		perror("server");
		return -1;
	}

	return 0;
}

void usage(char *name) {
	printf(
		"%s -[eh] PORT_NUMBER MOTD\n"
		"-e          Echo messages received on server's stdout\n"
		"-h          Displays help menu and returns EXIT_SUCCESS\n"
		"PORT_NUMBER Port number to listen on.\n"
		"MOTD        Message to display to the client when they connect\n",
		name
	);
}

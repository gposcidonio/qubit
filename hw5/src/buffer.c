#include "buffer.h"

void initBuffer(Buffer *buf) {
	buf->index = 0;
	buf->len = 1024;
	buf->str = malloc(sizeof(char) * buf->len);
}

void addChar(char c, Buffer *buf) {
	if (buf->index == buf->len) {
		char *newStr = malloc(2 * buf->len);
		memcpy(newStr, buf->str, buf->len);
		free(buf->str);
		buf->str = newStr;
		buf->len *= 2;
	}

	buf->str[buf->index++] = c;
}

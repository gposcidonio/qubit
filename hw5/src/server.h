#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "consts.h"
#include "server/quit.h"
#include "server/login.h"
#include "server/state.h"

int main(int argc, char **argv);
void *echo(void *arg);
int openListen(ServerState *state);
void usage(char *name);

#include "io.h"

void sendMessage(ClientList *client, char *message, ...) {
	va_list args;
	va_start(args, message);
	vdprintf(client->fd, message, args);
	va_end(args);
}

void vsendMessage(ClientList *client, char *message, va_list args) {
	vdprintf(client->fd, message, args);
}

void sendMessageToRoom(volatile ServerState *state, int roomId, char *message, ...){
	RoomList *room = getRoomForId(state, roomId);
	if(room != NULL){
		va_list args, argsCopy;
		va_start(args, message);
		//TODO implement getting clients based on IDs in room.
		IDList *curClientId = room->clientIds;
		while(curClientId != NULL){
			ClientList *curClient = getClientForId(state, curClientId->id);
			fflush(stdout);
			va_copy(argsCopy, args);
			vsendMessage(curClient, message, argsCopy);
			curClientId = curClientId->next;
		}
		va_end(args);
	}
}

void sendMessageToAll(volatile ServerState *state, char *message, ...){
	va_list args, argsCopy;
	va_start(args, message);
	ClientList *firstClient = state->clients;
	ClientList *curClient = firstClient;
	if (curClient != NULL) {
		do {
			fflush(stdout);
			va_copy(argsCopy, args);
			vsendMessage(curClient, message, argsCopy);
			curClient = curClient->next;
		} while (curClient != firstClient);
	}
	va_end(args);
}

void sendError(ClientList *client, ServerError errno) {
	dprintf(client->fd, "ERR %i %s \r\n", errno, getServerError(errno));
}

char *getServerError(ServerError errno) {
	// TODO fix some of these error messages
	switch (errno) {
		case NOT_OWNER:
			return "NOT OWNER";
			break;
		case INVALID_USER:
			return "INVALID USER";
			break;
		case INVALID_OPERATION:
			return "INVALID OPERATION";
			break;
		case INVALID_PASSWORD:
			return "INVALID PASSWORD";
			break;
		case SORRY:
			return "SORRY";
			break;
		case USER_EXISTS:
			return "USER EXISTS";
			break;
		case USER_DOES_NOT_EXIST:
			return "USER DOES NOT EXIST";
			break;
		case ROOM_EXISTS:
			return "ROOM EXISTS";
			break;
		case MAXIMUM_ROOMS_REACHED:
			return "MAXIMUM ROOMS REACHED";
			break;
		case ROOM_DOES_NOT_EXIST:
			return "ROOM DOES NOT EXIST";
			break;
		case USER_NOT_PRESENT:
			return "USER NOT PRESENT";
			break;
		case INTERNAL_SERVER_ERROR:
			return "INTERNAL SERVER ERROR";
			break;
		default:
			return NULL;
	}
}

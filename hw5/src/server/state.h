#pragma once

#include <stdlib.h>
#include <unistd.h>
#include <sqlite3.h>

#include "db.h"
#include "room-list.h"
#include "client-list.h"

#define WAITING_ROOM -1

typedef struct {
	int port;
	char *motd;
	int echoToStdout;
	int listenFd;
	ClientList *clients;
	RoomList *rooms;
	int numClients;
	int numRooms;
	int maxRooms;
	int echoThreadRunning;
	sqlite3 *db;
} ServerState;

ServerState *new_ServerState();
void free_ServerState(ServerState *state);

void addClient(ServerState *state, ClientList *client);
void removeClient(volatile ServerState *state, ClientList *client);
void addRoom(volatile ServerState *state, RoomList *room);
void removeRoom(volatile ServerState *state, RoomList *room);
int getFreeRoomId(volatile ServerState *state);
int getFreeClientId(volatile ServerState *state);
RoomList *getRoomForId(volatile ServerState *state, int roomId);
ClientList *getClientForId(volatile ServerState* state, int clientId);
ClientList *getClientForUsername(volatile ServerState *state, char* username);

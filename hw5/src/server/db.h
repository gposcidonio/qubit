#pragma once

#include <stdio.h>
#include <sqlite3.h>

#include "state.h"

#define SALT_LEN 5

typedef struct {
	char *username;
	char *passHash;
	char salt[SALT_LEN];
} ClientAuth;

ClientAuth *new_ClientAuth();
void free_ClientAuth(ClientAuth *auth);

int initDb(sqlite3 **pdb);

ClientAuth *getClientAuth(sqlite3 *db, char *name);
int getClientAuthCallback(
	void *arg,
	int argc,
	char **argv,
	char **colName
);
int addClientAuth(sqlite3 *db, ClientAuth *auth);
void cleanupDb(sqlite3 **pdb);

#include "room-list.h"

RoomList *new_RoomList(int roomId, int ownerId, char *roomName){
	RoomList *newRoom = malloc(sizeof(RoomList));
	newRoom->roomId = roomId;
	newRoom->ownerId = ownerId;
	newRoom->roomName = malloc(strlen(roomName) + 1);
	strcpy(newRoom->roomName, roomName);
	newRoom->isPrivate = 0;
	newRoom->password = NULL;
	newRoom->clientIds = NULL;
	return newRoom;
}

void free_RoomList(RoomList *list){
	assert(list != NULL);
	free_IDList(list->clientIds);
	free(list->roomName);
	free(list->password);
	free(list);
}

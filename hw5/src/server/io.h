#pragma once

#include <stdio.h>
#include <stdarg.h>

#include "client-list.h"
#include "state.h"

typedef enum {
	SORRY                 = 0,
	USER_EXISTS           = 1,
	USER_DOES_NOT_EXIST   = 2,
	ROOM_EXISTS           = 10,
	MAXIMUM_ROOMS_REACHED = 11,
	ROOM_DOES_NOT_EXIST   = 20,
	USER_NOT_PRESENT      = 30,
	NOT_OWNER             = 40,
	INVALID_USER          = 41,
	INVALID_OPERATION     = 60,
	INVALID_PASSWORD      = 61,
	INTERNAL_SERVER_ERROR = 100
} ServerError;

void sendMessage(ClientList *client, char *message, ...);
void vsendMessage(ClientList *client, char *message, va_list args);
void sendMessageToRoom(volatile ServerState *state, int roomId, char *message, ...);
void sendMessageToAll(volatile ServerState *state, char *message, ...);
void sendError(ClientList *client, ServerError errno);
char *getServerError(ServerError errno);

#include "echo.h"

void *echo(void *arg) {
	assert(arg != NULL);
	volatile ServerState *state = arg;

	ClientList *client = state->clients;
	while (client != NULL) {
		// remove client if it's been flagged
		if (client->shouldBeRemoved) {
			printf("client disconnected\n");
			removeClient(state, client);
			client = state->clients;
			continue;
		}

		// check if client has input
		int ready = hasInput(client->fd);
		if (ready == -1) {
			perror("server");
		} else if (ready == 0) {
			client = client->next;
		} else {
			// read from client
			char c;
			int bytesRead = readChar(client->fd, &client->buf);
			if (bytesRead == 1) {
				c = client->buf.str[client->buf.index - 1];
				if (c == '\n' && client->buf.index >= 3) {
					// if we hit the end-of-command, parse the command
					addChar('\0', &client->buf);
					parseClientCommand(state, client);
				}

				client = client->next;
			} else if (bytesRead == 0) {
				// EOF, client hung up
				client->shouldBeRemoved = 1;
				client = state->clients;
			} else {
				// an error occurred
				perror("server");
			}
		}
	}

	state->echoThreadRunning = 0;
	return NULL;
}

#include "login.h"

void *login(void *arg) {
	LoginStruct *loginStruct = arg;
	ServerState *state = loginStruct->state;
	ClientList *client = new_ClientList(loginStruct->clientFd);
	free(loginStruct);

	printf("starting login of new client\n");

	// get and check aloha
	char *line = dgetln(client->fd);
	if (strcmp(line, CLIENT_ALOHA) != 0) {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		free_ClientList(client);
		free(line);
		return NULL;
	}
	free(line);

	// send ahola
	sendMessage(client, SERVER_AHOLA);

	// process IAM or IAMNEW
	Command cmd = dgetCommand(client->fd);
	int wasError = 0;
	if (strcmp(cmd.verb, "IAM") == 0) {
		wasError = !loginClient(state, client, cmd.message);
	} else if (strcmp(cmd.verb, "IAMNEW") == 0) {
		wasError = !registerClient(state, client, cmd.message);
	} else {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		wasError = 1;
	}

	free(cmd.verb);
	if (wasError) {
		return NULL;
	}

	sendMessage(client, "HI %s \r\n", client->username);

	// send motd
	sendMessage(client, "ECHO server %s \r\n", state->motd);

	if (!state->echoThreadRunning) {
		state->echoThreadRunning = 1;
		pthread_t tid;
		pthread_create(&tid, NULL, echo, state);
	}

	return NULL;
}

int loginClient(ServerState *state, ClientList *client, char *name) {
	// check if client exists
	ClientAuth *auth = getClientAuth(state->db, name);
	if (auth == NULL) {
		sendError(client, USER_DOES_NOT_EXIST);
		sendMessage(client, "BYE \r\n");
		return 0;
	}

	// check if client is already logged in
	if (getClientForUsername(state, name) != NULL) {
		sendError(client, SORRY);
		sendMessage(client, "BYE \r\n");
		return 0;
	}

	// get password
	sendMessage(client, "AUTH %s \r\n", name);
	Command cmd = dgetCommand(client->fd);
	if (strcmp(cmd.verb, "PASS") != 0) {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		free(cmd.verb);
		return 0;
	}

	// check password
	char *password = cmd.message;
	char *hash = hashPassword(password, auth->salt);
	if (strncmp(hash, auth->passHash, SHA_DIGEST_LEN) != 0) {
		sendError(client, INVALID_PASSWORD);
		sendMessage(client, "BYE \r\n");
		free(cmd.verb);
		return 0;
	}
	free_ClientAuth(auth);

	// log client in
	printf("client %s connected\n", name);
	client->username = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(client->username, name);
	addClient(state, client);

	return 1;
}

int registerClient(ServerState *state, ClientList *client, char *name) {
	// check if client already exists
	ClientAuth *auth = getClientAuth(state->db, name);
	if (auth != NULL) {
		sendError(client, USER_EXISTS);
		sendMessage(client, "BYE \r\n");
		free_ClientAuth(auth);
		return 0;
	}

	if(strlen(name) > MAX_USERNAME_LEN){
		sendError(client, SORRY);
		sendMessage(client, "BYE \r\n");
		return 0;
	}


	sendMessage(client, "HINEW %s \r\n", name);
	auth = new_ClientAuth();
	auth->username = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(auth->username, name);
	client->username = malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(client->username, name);

	// get password
	Command cmd = dgetCommand(client->fd);
	if (strcmp(cmd.verb, "NEWPASS") != 0) {
		sendError(client, INVALID_OPERATION);
		sendMessage(client, "BYE \r\n");
		return 0;
	}

	// validate password
	if (!isValidPassword(cmd.message)) {
		sendError(client, INVALID_PASSWORD);
		sendMessage(client, "BYE \r\n");
		return 0;
	}

	// hash password
	if (
		RAND_bytes(
			(unsigned char *)auth->salt,
			SALT_LEN
		) != 1
	) {
		sendError(client, INTERNAL_SERVER_ERROR);
		sendMessage(client, "BYE \r\n");
		return 0;
	}
	char *hash = hashPassword(cmd.message, auth->salt);
	auth->passHash = malloc(SHA_DIGEST_LEN);
	strncpy(auth->passHash, hash, SHA_DIGEST_LEN);
	addClient(state, client);
	addClientAuth(state->db, auth);
	free_ClientAuth(auth);

	return 1;
}

int isValidPassword(char *password) {
	size_t len = 0;
	int uppercases = 0;
	int symbols = 0;
	int numbers = 0;

	while (*password) {
		len++;
		char c = (*(password++));
		if (0x40 < c && c <= 0x5a) {
			uppercases |= 1;
		} else if (0x30 <= c && c < 0x3a) {
			numbers |= 1;
		} else if (c < 0x61 || 0x7a < c) {
			symbols |= 1;
		}
	}

	return len >= 5 && uppercases && symbols && numbers;
}

char *hashPassword(char *password, char *salt) {
	size_t len = strlen(password) + SALT_LEN;
	char *saltedPass = malloc(len + 1);
	strcpy(saltedPass, password);
	strncat(saltedPass, salt, SALT_LEN);

	char *hash = (char *)SHA256(
		(unsigned char *)saltedPass,
		len,
		NULL
	);

	free(saltedPass);
	return hash;
}

int isRegistered(char *name) {
	return 0;
}

#include "id-list.h"

IDList *new_IDList(int id){
	IDList *node = malloc(sizeof(IDList));
	node->id = id;
	node->next = NULL;
	return node;
}

void free_IDList(IDList *node){
	IDList *temp;
	while(node != NULL){
		temp = node->next;
		free(node);
		node = temp;
	}
}

int removeIDFromIDList(int id, IDList **list){
	int errorOcurred = 0;
	IDList *node;
	for(node = *list; node != NULL; node = node->next){
		if(node->id == id){
			// The only time this will ever trigger first
			// is if we are at the start of the list
			if(node == *list){
				// sanity check
				*list = (*list)->next;
				node->next = NULL;
				free_IDList(node);
				return errorOcurred;
			}
		} else if(node->next->id == id){
			IDList *temp = node->next;
			node->next = node->next->next;
			temp->next = NULL;
			free_IDList(temp);
			return errorOcurred;
		}
	}
	errorOcurred = 1;
	return errorOcurred;
}

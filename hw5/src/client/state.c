#include "state.h"

ClientState *new_ClientState() {
	ClientState *state = malloc(sizeof(ClientState));
	state->serverFd = -1;
	state->newUser = 0;
	state->username = NULL;
	state->serverIp = NULL;
	state->serverPort = NULL;
	initBuffer(&state->stdinBuf);
	initBuffer(&state->serverBuf);
	state->inRoom = 0;
	return state;
}

void free_ClientState(ClientState *state) {
	if (state->serverFd >= 0) close(state->serverFd);
	free(state);
}

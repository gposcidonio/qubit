#pragma once

#include <stdio.h>
#include <string.h>

#include "io.h"
#include "state.h"
#include "../io.h"

void parseServerCommand(ClientState *state, Buffer buf);
void parseClientCommand(ClientState *state, Buffer buf);
void *refreshUserList(void *vargp);

#pragma once

#include <stdio.h>

#include "io.h"
#include "state.h"
#include "../io.h"

int authUser(ClientState *state, char *username);
int registerUser(ClientState *state, char *username);
int loginUser(ClientState *state, char *username);

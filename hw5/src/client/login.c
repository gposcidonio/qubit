#include "login.h"

int authUser(ClientState *state, char *username) {
	// initiate three-way handshake
	printf("connecting to server...\n");
	dprintf(state->serverFd, "%s", CLIENT_ALOHA);

	// wait for server to ahola back
	char *line = dgetln(state->serverFd);
	if (strcmp(line, SERVER_AHOLA) != 0) {
		fprintf(stderr, "server failed handshake: %s\n", line);
		return -1;
	}

	// successfully handshook, auth and continue
	if (state->newUser) {
		if (!registerUser(state, state->username)) {
			fprintf(stderr, "auth failed\n");
			return -1;
		}
	} else {
		if (!loginUser(state, state->username)) {
			fprintf(stderr, "auth failed\n");
			return -1;
		}
	}

	return 0;
}

int loginUser(ClientState *state, char *username) {
	dprintf(state->serverFd, "IAM %s \r\n", username);

	Command cmd = dgetCommand(state->serverFd);
	if (cmd.verb == NULL || strcmp(cmd.verb, "AUTH") != 0) {
		printf("server saysa: %s %s\n", cmd.verb, cmd.message);
		free(cmd.verb);
		return 0;
	}
	free(cmd.verb);

	// get password from user
	printf("input password: ");
	char *password = NULL;
	size_t passLen;
	if (cmd.verb == NULL || getline(&password, &passLen, stdin) == -1) {
		perror(NULL);
		return 0;
	}
	*strchr(password, '\n') = '\0';

	dprintf(state->serverFd, "PASS %s \r\n", password);

	// confirm password was right
	cmd = dgetCommand(state->serverFd);
	if (strcmp(cmd.verb, "HI") != 0) {
		printf("server says: %s %s\n", cmd.verb, cmd.message);
		free(cmd.verb);
		return 0;
	}
	free(cmd.verb);

	return 1;
}

int registerUser(ClientState *state, char *username) {
	dprintf(state->serverFd, "IAMNEW %s \r\n", username);

	Command cmd = dgetCommand(state->serverFd);
	if (cmd.verb == NULL || strcmp(cmd.verb, "HINEW") != 0) {
		printf("server saysa: %s %s\n", cmd.verb, cmd.message);
		free(cmd.verb);
		return 0;
	}
	free(cmd.verb);

	// get password from user
	printf("input new password: ");
	char *password = NULL;
	size_t passLen;
	if (getline(&password, &passLen, stdin) == -1) {
		perror(NULL);
		return 0;
	}
	*strchr(password, '\n') = '\0';

	dprintf(state->serverFd, "NEWPASS %s \r\n", password);

	// confirm password was right
	cmd = dgetCommand(state->serverFd);
	if (cmd.verb == NULL || strcmp(cmd.verb, "HI") != 0) {
		printf("server says: %s %s\n", cmd.verb, cmd.message);
		free(cmd.verb);
		return 0;
	}
	free(cmd.verb);

	return 1;
}

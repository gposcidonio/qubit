#include "quit.h"

void quit(ClientState *state, int exitStatus) {
	free_ClientState(state);
	endwin();
	exit(exitStatus);
}

#include "io.h"

void initIo(ClientState *state) {
	initscr();
	start_color();

	int maxX, maxY;
	getmaxyx(stdscr, maxY, maxX);

	state->textWindow = subwin(stdscr, maxY - 3, maxX - 22, 0, 1);
	scrollok(state->textWindow, TRUE);
	wmove(state->textWindow, 1, 0);

	state->inputWindow = subwin(stdscr, 3, maxX - 22, maxY - 3, 1);
	wmove(state->inputWindow, 1, 2);

	state->userListWindow = subwin(stdscr, maxY, 21, 0, maxX - 21);

	display(state);
}

void displayMessage(ClientState *state, char *message, ...) {
	va_list args;
	va_start(args, message);
	waddch(state->textWindow, ' ');
	waddch(state->textWindow, ' ');
	vw_printw(state->textWindow, message, args);
	va_end(args);
	box(state->textWindow, '|', '-');

	display(state);
}

void displayColoredMessage(ClientState *state, char *color, char *message, ...){
	init_pair(1, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(2, COLOR_CYAN, COLOR_BLACK);
	init_pair(3, COLOR_RED, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_YELLOW, COLOR_BLACK);
	init_pair(6, COLOR_WHITE, COLOR_BLACK);

	int colorNum = 6;
	if(strcmp(color, "purple") == 0){
		colorNum = 1;
	} else if(strcmp(color, "cyan") == 0){
		colorNum = 2;
	} else if(strcmp(color, "red") == 0){
		colorNum = 3;
	} else if(strcmp(color, "blue") == 0){
		colorNum = 4;
	} else if(strcmp(color, "yellow") == 0){
		colorNum = 5;
	}



	wattron(state->textWindow, COLOR_PAIR(colorNum));

	va_list args;
	va_start(args, message);
	waddch(state->textWindow, ' ');
	waddch(state->textWindow, ' ');
	vw_printw(state->textWindow, message, args);
	va_end(args);

	wattroff(state->textWindow, COLOR_PAIR(colorNum));
	box(state->textWindow, '|', '-');

	display(state);
}

void display(ClientState *state) {
	mvwaddch(state->inputWindow, 1, 2, '\n');
	wmove(state->inputWindow, 1, 2);
	if (state->stdinBuf.index > 0) {
		// display input line
		addChar('\0', &state->stdinBuf);
		state->stdinBuf.index--;
		mvwprintw(state->inputWindow, 1, 2, "%s", state->stdinBuf.str);
	}

	// display windows
	box(state->textWindow, '|', '-');
	box(state->inputWindow, '|', '-');
	box(state->userListWindow, '|', '-');

	wrefresh(state->textWindow);
	wrefresh(state->userListWindow);
	wrefresh(state->inputWindow);

}

void displayUserList(ClientState *state, char *users[], int numUsers){
	int i = 0;

	werase(state->userListWindow);
	while (i < numUsers) {
		char *currentUser = users[i];
		mvwprintw(state->userListWindow, i + 1, 2, "%s", currentUser);
		i++;
	}

	display(state);
}

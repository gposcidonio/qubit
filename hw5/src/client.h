#pragma once

#include <time.h>
#include <netdb.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "io.h"
#include "consts.h"
#include "client/io.h"
#include "client/quit.h"
#include "client/login.h"
#include "client/parse.h"
#include "client/state.h"

int main(int argc, char **argv);
int connectToServer(ClientState *state, char *ip, char *port);
void usage(char *binName);
